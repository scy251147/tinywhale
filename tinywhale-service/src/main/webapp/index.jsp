<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Offheap Cache Ask and Answer</title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <style type="text/css">
        .hstyle{font-size: 13px;margin:0 auto;width:800px;}
        .sstyle{font-size: 13px;margin:0 auto;width:600px;margin-top:20px;}
    </style>
    <script type="application/javascript">
        $(document).ready(function(){
            $("#execBtn").click(function(){
                getExecResult();
            });
            $("#exprBtn").click(function(){
                getExprResult();
            });
            $("#monitorBtn").click(function(){
                getMonitorResult();
            });
        })

        var getExecResult = function() {
            var inputUrl = "/offheap/cmd";
            var inputVal = $("#command").val();
            var inputSize  = inputVal.split(' ')[0].length;
            var inputCommand = inputUrl + "?callback=test&command=" + inputVal.split(' ')[0].trim() + "&args=" + inputVal.substring(inputSize, inputVal.length).trim();
            $.get(inputCommand, function (data) {
                $("#resultCanvas").html(JSON.stringify(data));
            });
        }

        var getExprResult = function() {
            var inputUrl = "/offheap/expr";
            var inputCommand = inputUrl + "?callback=test";
            $.get(inputCommand, function (data) {
                $("#resultCanvas").html(JSON.stringify(data));
            });
        }

        var getMonitorResult = function() {
            var inputUrl = "/offheap/monitor";
            var inputCommand = inputUrl + "?callback=test";
            $.get(inputCommand, function (data) {
                var newData = data.replace(/\r\n/g,"<br>");
                $("#resultCanvas").html(newData);
            });
        }

    </script>
</head>
<body>
<div class="hstyle">
    <label>Command&nbsp;:&nbsp;</label>
    <input id="command" name="command" type="text" value="">
    <button id="execBtn">执行</button>
    <button id="exprBtn">6w个随机键生成(过期时间在10秒~130秒)</button>
    <button id="monitorBtn">监视</button>
</div>
<div class="sstyle" id="resultCanvas"></div>
</body>
</html>