package com.tw.service.service;


import com.tw.client.HelloService;
import com.tw.core.TinyWhaleFire;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-02-20 21:16
 */
@TinyWhaleFire(HelloService.class)
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello(String name) {
        return "Hello " + name;
    }
}
