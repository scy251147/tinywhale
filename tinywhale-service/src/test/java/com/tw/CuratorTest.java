package com.tw;

import com.google.common.collect.Maps;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.retry.RetryNTimes;
import org.apache.curator.x.discovery.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.List;
import java.util.Map;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-06-05 10:21
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-config.xml"})
public class CuratorTest {

    @Test
    public void serviceRegistryTest() throws Exception {

        String providerHost = "127.0.0.1";
        String providerInterface = "com.tw.client.HelloService";
        int providerPort = 8024;

        CuratorFramework curatorFramework = CuratorFrameworkFactory.newClient("127.0.0.1:2181", new RetryNTimes(5, 1000));
        curatorFramework.start();
        System.out.println("---->连接成功");

        ServiceDiscovery serviceDiscovery = ServiceDiscoveryBuilder.builder(ServiceInstance.class)
                .client(curatorFramework)
                .basePath("/registry")
                .watchInstances(true)
                .build();

        serviceDiscovery.start();

        ServiceInstance serviceInstance = ServiceInstance.builder()
                .name(providerInterface)
                .address(providerHost)
                .port(providerPort)
                .serviceType(ServiceType.DYNAMIC_SEQUENTIAL)
                .build();

        serviceDiscovery.registerService(serviceInstance);

        List<ServiceInstance> providerList = (List<ServiceInstance>) serviceDiscovery.queryForInstances(providerInterface);

        for (ServiceInstance provider : providerList) {
            System.out.println(provider.getId() + "-->" + provider.getName() + "--" + provider.getAddress() + ":" + provider.getPort());
        }

        System.in.read();
    }

    @Test
    public void serviceCacheTest() throws Exception {

        CuratorFramework curatorFramework = CuratorFrameworkFactory.newClient("127.0.0.1:2181", new RetryNTimes(5, 1000));
        curatorFramework.start();
        System.out.println("---->连接成功");

        TreeCache treeCache = new TreeCache(curatorFramework, "/registry");
        treeCache.start();
        treeCache.getListenable().addListener((zkClient, event) -> {
            switch (event.getType()) {
                case NODE_ADDED:
                    System.out.println("添加节点" + event.getData().getPath() + ":" + new String(event.getData().getData()));
                    break;
                case NODE_UPDATED:
                    System.out.println("更新节点");
                    break;
                case NODE_REMOVED:
                    System.out.println("删除节点");
                    break;
                default:
                    break;
            }
        });

        System.in.read();
    }

    @Test
    public void testx(){
        mybase a = new mya();
        a.add();

        mybase b = new myb();
        b.add();

        System.out.println(a.map);
        System.out.println(b.map);
    }


    interface mycache{
        Map<String, String> map = Maps.newConcurrentMap();
    }

    abstract  class mybase  implements mycache{
        abstract void add();
    }

    class mya extends mybase{
        public void add (){
            map.put("a","1");
        }

    }

    class myb extends mybase {
        public void add() {
            map.put("b", "1");
        }
    }
}
