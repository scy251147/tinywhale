package com.tw;

import com.tw.client.HelloService;
import com.tw.service.service.HelloServiceImpl;
import javassist.ClassPool;
import javassist.CtClass;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.TimeUnit;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-04-13 18:35
 */
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Thread)
public class ProxyBenchmark {

    private HelloService serviceBean;

    @Setup
    public void init() {
        serviceBean = new HelloServiceImpl();
    }

    @Benchmark
    @GroupThreads(4)
    public void proxyByJdk() {
        Class<?> interfaceClass = HelloService.class;
        HelloService helloService = (HelloService) Proxy.newProxyInstance(interfaceClass.getClassLoader()
                , new Class<?>[]{interfaceClass}
                , new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                        Class<?> serviceClass = interfaceClass;
                        String methodName = method.getName();
                        Class<?>[] parameterTypes = method.getParameterTypes();
                        Object[] parameters = args;

                        FastClass serviceFastClass = FastClass.create(serviceClass);
                        FastMethod serviceFastMethod = serviceFastClass.getMethod(methodName, parameterTypes);
                        Object result = serviceFastMethod.invoke(serviceBean, parameters);
                        return result;
                    }
                });
        helloService.hello("xxxxx");
    }

    @Benchmark
    @GroupThreads(4)
    public void proxyByCglib() {
        Class<?> interfaceClass = HelloService.class;
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                Class<?> serviceClass = interfaceClass;
                String methodName = method.getName();
                Class<?>[] parameterTypes = method.getParameterTypes();
                Object[] parameters = args;

                FastClass serviceFastClass = FastClass.create(serviceClass);
                FastMethod serviceFastMethod = serviceFastClass.getMethod(methodName, parameterTypes);
                Object result = serviceFastMethod.invoke(serviceBean, parameters);
                return result;
            }
        });
        enhancer.setInterfaces(new Class[]{interfaceClass});
        HelloService helloService = (HelloService) enhancer.create();
        String rst = helloService.hello("xxxxx");
    }

    @Benchmark
    @GroupThreads(4)
    public void proxyByDirect() {
        String rst = serviceBean.hello("xxxxxx");
    }

    /**
     * javassist只用字节码方式，不要用动态proxy方式，比jdk的proxy还要慢
     * @throws Exception
     */
    @Benchmark
    @GroupThreads(4)
    public void proxyByJavassist() throws Exception{
        ClassPool mPool = new ClassPool(true);
        CtClass mCtc = mPool.makeClass(HelloService.class.getName() + "JavaassistProxy");
        mCtc.addInterface(mPool.get(HelloService.class.getName()));
        Class<?> pc = mCtc.toClass();
        HelloService bytecodeProxy = (HelloService) pc.newInstance();
        bytecodeProxy.hello("xxxx");
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(ProxyBenchmark.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(opt).run();
    }

}
