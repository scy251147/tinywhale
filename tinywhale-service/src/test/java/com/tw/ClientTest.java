package com.tw;

import com.tw.client.HelloService;
import com.tw.core.TinyWhaleProxy;
import com.tw.message.NettyResponse;
import com.tw.service.service.HelloServiceImpl;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewConstructor;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-03-26 18:39
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-config.xml"})
public class ClientTest {

    @Autowired
    private TinyWhaleProxy rpcProxy;

    @Test
    public void testClient() {
        for (int i = 0; i < 1000; i++) {
            HelloService helloService = rpcProxy.create(HelloService.class);
            String result = helloService.hello("sdfsdfsdf" + i);
            System.out.println("xxxxxxxxxxxxxxxx------>" + result);
        }
    }

    @Test
    public void testCglib(){
        Class<?> interfaceClass = HelloService.class;
        HelloService helloServicex = new HelloServiceImpl();
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                Class<?> serviceClass = interfaceClass;
                String methodName = method.getName();
                Class<?>[] parameterTypes = method.getParameterTypes();
                Object[] parameters = args;

                Object serviceBean = helloServicex;
                FastClass serviceFastClass = FastClass.create(serviceClass);
                FastMethod serviceFastMethod = serviceFastClass.getMethod(methodName, parameterTypes);
                Object result = serviceFastMethod.invoke(serviceBean, parameters);
                return result;
            }
        });
        enhancer.setInterfaces(new Class[]{interfaceClass});
        HelloService helloService = (HelloService) enhancer.create();
        String rst = helloService.hello("xxxxx");
        System.out.println(rst);
    }

//    @Test
//    public void testJavassist() throws Exception {
//        Class<?> interfaceClass = HelloService.class;
//        HelloService helloServicex = new HelloServiceImpl();
//
//        ProxyFactory proxyFactory = new ProxyFactory();
//        proxyFactory.setInterfaces(new Class[]{HelloService.class});
//        Class<?> proxyClass = proxyFactory.createClass();
//        HelloService javassistProxy = (HelloService) proxyClass.newInstance();
//        ((ProxyObject) javassistProxy).setHandler(new MethodHandler() {
//            @Override
//            public Object invoke(Object proxy, Method method, Method proceed, Object[] args) throws Throwable {
//                Class<?> serviceClass = interfaceClass;
//                String methodName = method.getName();
//                Class<?>[] parameterTypes = method.getParameterTypes();
//                Object[] parameters = args;
//
//                Object serviceBean = helloServicex;
//                FastClass serviceFastClass = FastClass.create(serviceClass);
//                FastMethod serviceFastMethod = serviceFastClass.getMethod(methodName, parameterTypes);
//                Object result = serviceFastMethod.invoke(serviceBean, parameters);
//                return result;
//
//                return method.invoke(proceed)
//            }
//        });
//    }
}
