package com.tw.core;

import com.tw.message.NettyMessage;
import com.tw.message.NettyRequest;
import com.tw.message.NettyResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * TinyWhaleFuture，仿CountDownLatch写法，此为Doug Lea常用的一种扩展方式
 * 可以通过翻阅CountDownLatch源码来了解。
 *
 * http://gee.cs.oswego.edu/dl/papers/aqs.pdf  此为Doug Lea的AbstractQueuedSynchronizer设计原理全文
 *
 * AbstractQueuedSynchronizer为同步的核心类，必须要会使用的
 *
 */
public class TinyWhaleFuture implements Future<Object> {

    private static final Logger logger = LoggerFactory.getLogger(TinyWhaleFuture.class);

    /**
     * 同步
     */

    private Sync sync;

    /**
     *请求消息体
     */
    private NettyMessage<NettyRequest> request;

    /**
     * 响应消息体
     */
    private NettyMessage<NettyResponse> response;

    /**
     * 开始时间，用于计算超时
     */
    private long startTime;

    /**
     * 回调队列
     */
    private List<TinyWhaleAsyncCallback> callbacks = new ArrayList<>();

    /**
     * 结果检测锁
     */
    private ReentrantLock lock = new ReentrantLock();

    /**
     * 带参构造
     *
     * @param request
     */
    public TinyWhaleFuture(NettyMessage<NettyRequest> request) {
        this.sync = new Sync();
        this.request = request;
        this.startTime = System.currentTimeMillis();
    }

    /**
     * 是否完成
     *
     * @return
     */
    @Override
    public boolean isDone() {
        return sync.isDone();
    }

    /**
     * 返回结果，阻塞等待
     *
     * @return
     */
    @Override
    public Object get() {
        //阻塞等待，一直等到返回结果到来，处于阻塞状态
        sync.acquire(-1);
        if (this.response != null) {
            return this.response.getBody();
        } else {
            return null;
        }
    }

    /**
     * 返回结果，阻塞等待，带有超时机制
     *
     * @param timeout
     * @param unit
     * @return
     * @throws InterruptedException
     */
    @Override
    public Object get(long timeout, TimeUnit unit) throws InterruptedException {
        //阻塞到获取锁返回true表示获得了锁，或者被打断抛出异常，或者到超时，返回false表示没有获得锁。
        boolean success = sync.tryAcquireNanos(-1, unit.toNanos(timeout));
        if (success) {
            //获得锁成功，表明已经有结果返回或者还没有提交处理，中断处理了
            if (this.response != null) {
                return this.response.getBody();
            } else {
                return null;
            }
        } else {
            throw new RuntimeException("请求超时,RequestId:" + this.request.getBody().getRequestId() + ". RequestBody:" + this.request.getBody());
        }
    }

    /**
     * 是否能够中断一个rpc请求消息
     *
     * @return
     */
    @Override
    public boolean isCancelled() {
        throw new UnsupportedOperationException();
    }

    /**
     * 中断一个rpc请求消息
     *
     * @param mayInterruptIfRunning 中断的时候是否正在有运行的任务
     * @return
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        throw new UnsupportedOperationException();
    }

    /**
     * 收到rpc消息返回。
     * 收到一个rpc消息返回的时候，首先将消息保存到本地，然后将同步锁释放掉。{@link Sync#release(int)}
     *
     * @param response
     */
    public void done(NettyMessage<NettyResponse> response) {
        this.response = response;
        sync.release(1);
        invokeCallbacks();
        // Threshold
        long responseTime = System.currentTimeMillis() - startTime;
        logger.info("耗时:"+responseTime);
        /**
         * 远程调用返回接口最大时长
         */
        long responseTimeThreshold = 200;
        if (responseTime > responseTimeThreshold) {
            logger.info("服务端响应慢. RequestId:" + response.getBody().getRequestId() + ". 响应时间:" + responseTime + "ms");
        }
    }

    /**
     * 是否超时，当网络状态比较差或者负载比较高的时候，一条rpc请求消息可能会延迟，
     * 可以利用延迟策略来决定消息是否重新发送处理。
     *
     * @return
     */
    public boolean isTimeout() {
        long responseTime = System.currentTimeMillis() - startTime;
        if (responseTime >= 3000) {
            return true;
        }
        return false;
    }

    /**
     * 调用回调函数。当一个rpc 请求消息有多个回调函数调用的时候
     * 需要把回调函数接口存放到一个集合中，才用可重入锁{@link ReentrantLock}
     * 来解决并发带来的问题
     */
    private void invokeCallbacks() {
        lock.lock();
        try {
            for (final TinyWhaleAsyncCallback callback : callbacks) {
                runCallback(callback);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * 添加回调函数，添加回调函数即对当前rpc请求返回结果进行
     * 监听处理
     *
     * @param callback
     * @return
     */
    public TinyWhaleFuture addCallback(TinyWhaleAsyncCallback callback) {
        lock.lock();
        try {
            if (isDone()) {
                logger.info("已获取调用结果");
                runCallback(callback);
            } else {
                logger.info("等待获取调用结果");
                this.callbacks.add(callback);
            }
            //不管怎么样，都要释放锁
        } finally {
            lock.unlock();
        }
        return this;
    }

    /**
     * 运行一个回调。如何获得一个执行结果呢？需要在
     * 的回调函数里面获取返回结果。最后将结果类型是否成功提交给调用线程的{@link TinyWhaleAsyncCallback}对象，
     * {@link TinyWhaleAsyncCallback} 对象自己去实现返回成功的业务逻辑和返回失败的业务逻辑
     *
     * @param callback
     */
    private void runCallback(final TinyWhaleAsyncCallback callback) {
        final NettyMessage<NettyResponse> res = this.response;
        callback.success(res.getBody());
    }

    /**
     * 实现异步回调的关键核心
     */
    static class Sync extends AbstractQueuedSynchronizer {

        private static final long serialVersionUID = 1L;

        private final int done = 1;
        private final int pending = 0;

        @Override
        protected boolean tryAcquire(int acquires) {
            return getState() == done;
        }

        /**
         * CAS操作，保证原子性
         *
         * @param releases
         * @return
         */
        @Override
        protected boolean tryRelease(int releases) {
            if (getState() == pending) {
                if (compareAndSetState(pending, done)) {
                    return true;
                }
            }
            return false;
        }

        public boolean isDone() {
            getState();
            return getState() == done;
        }
    }
}