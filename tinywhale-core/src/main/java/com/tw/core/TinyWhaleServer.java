package com.tw.core;

import com.tw.core.serverManage.ServerRegistry;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import com.tw.server.NettyServer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shichaoyang
 * @Description: 服务端
 * @date 2019-02-21 21:21
 */
public class TinyWhaleServer implements ApplicationContextAware, InitializingBean {

    /**
     * 服务器地址，形如 ip:port
     */
    private String serverAddress;

    /**
     * 服务注册
     */
    private ServerRegistry serviceRegistry;

    /**
     * 存放接口名与服务对象之间的映射关系
     */
    private Map<String, Object> serverBeans = new HashMap<>();

    /**
     * 获取所有带有TinyWhaleFire注解的bean
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, Object> serviceBeanMap = applicationContext.getBeansWithAnnotation(TinyWhaleFire.class);
        if (serviceBeanMap != null && serviceBeanMap.size() > 0) {
            for (Object serviceBean : serviceBeanMap.values()) {
                String interfaceName = serviceBean.getClass().getAnnotation(TinyWhaleFire.class).value().getName();
                serverBeans.put(interfaceName, serviceBean);
            }
        }
    }

    /**
     * 所有bean加载后，启动server
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        NettyServer nettyServer = new NettyServer(serverAddress, serviceRegistry, serverBeans);
        nettyServer.bind();
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public void setServiceRegistry(ServerRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}