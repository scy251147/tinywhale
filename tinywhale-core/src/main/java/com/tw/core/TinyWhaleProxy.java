package com.tw.core;

import com.tw.client.NettyClient;
import com.tw.core.serverManage.ServerDiscovery;
import com.tw.message.MessageType;
import com.tw.message.NettyMessage;
import com.tw.message.NettyRequest;
import com.tw.message.NettyResponse;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author shichaoyang
 * @Description: 服务代理
 * @date 2019-02-24 15:03
 */
public class TinyWhaleProxy {

    /**
     * 日志记录
     */
    private static final Logger logger = LoggerFactory.getLogger(TinyWhaleProxy.class);

    /**
     * 服务发现
     */
    private ServerDiscovery serviceDiscovery;

    /**
     * 创建客户端代理
     *
     * @param interfaceClass
     * @param <T>
     * @return
     */
    public <T> T create(Class<?> interfaceClass) {
        /**
         T result = (T) Proxy.newProxyInstance(interfaceClass.getClassLoader()
         , new Class<?>[]{interfaceClass}
         , (proxy, method, args) -> {
         NettyRequest request = new NettyRequest();
         request.setRequestId(UUID.randomUUID().toString());
         request.setClassName(method.getDeclaringClass().getName());
         request.setMethodName(method.getName());
         request.setParameterTypes(method.getParameterTypes());
         request.setParameterValues(args);

         NettyMessage nettyMessage = new NettyMessage();
         nettyMessage.setType(MessageType.SERVICE_REQ.value());
         nettyMessage.setBody(request);

         if (serviceDiscovery != null) {
         serverAddress = serviceDiscovery.discover();
         }
         String[] array = serverAddress.split(":");
         String host = array[0];
         int port = Integer.parseInt(array[1]);

         NettyClient client = new NettyClient(host, port);
         NettyMessage nettyResponse = client.send(nettyMessage);
         if (nettyResponse != null) {
         return JSON.toJSONString(nettyResponse.getBody());
         } else {
         return null;
         }
         });
         return result;
         **/

        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {

                //构建请求
                NettyRequest request = new NettyRequest();
                request.setRequestId(UUID.randomUUID().toString());
                request.setClassName(method.getDeclaringClass().getName());
                request.setMethodName(method.getName());
                request.setParameterTypes(method.getParameterTypes());
                request.setParameterValues(args);

                //填充消息体
                NettyMessage nettyMessage = new NettyMessage();
                nettyMessage.setType(MessageType.SERVICE_REQ.value());
                nettyMessage.setBody(request);

                //获取服务端套接字
                String serverSocket = serviceDiscovery.discover(request.getClassName());
                if(StringUtils.isEmpty(serverSocket)) {
                    logger.error("server socket为空");
                    return null;
                }

                String[] array = serverSocket.split(":");
                String host = array[0];
                int port = Integer.parseInt(array[1]);

                //发送消息并获取结果
                NettyClient client = new NettyClient(host, port);
                TinyWhaleFuture nettyResponse = client.send(nettyMessage);
                NettyResponse rst = (NettyResponse) nettyResponse.get(5000, TimeUnit.MILLISECONDS);
                return rst.getResult();
            }
        });
        enhancer.setInterfaces(new Class[]{interfaceClass});
        T proxyInstance = (T) enhancer.create();
        return proxyInstance;
    }

    public void setServiceDiscovery(ServerDiscovery serviceDiscovery) {
        this.serviceDiscovery = serviceDiscovery;
    }
}
