package com.tw.core.loadBalance;

import java.util.List;

/**
 * @author shichaoyang
 * @Description: 轮询
 * @date 2019-04-04 17:08
 */
public class LoadBalancePerRoundrobin implements LoadBalance {

    private int tempIndex = -1;

    @Override
    public String selectService(List<String> services) {
        //服务端列表不能为空
        if (services == null || services.size() == 0) {
            return null;
        }

        int size = services.size();

        //单个元素直接返回
        if (size == 1) {
            return services.get(0);
        }

        //轮询操作
        tempIndex = tempIndex + 1;
        if (tempIndex > size) {
            tempIndex = 0;
        }

        return services.get(tempIndex);
    }
}
