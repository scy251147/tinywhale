package com.tw.core.loadBalance;

import java.util.List;

/**
 * @author shichaoyang
 * @Description: 负载均衡基类
 * @date 2019-04-04 17:07
 */
public interface LoadBalance {

    /**
     * 在服务端列表中选取一个服务供客户端调用
     * @param services
     * @return
     */
    String selectService(List<String> services);

}
