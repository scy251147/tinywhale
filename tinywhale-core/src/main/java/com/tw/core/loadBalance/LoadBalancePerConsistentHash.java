package com.tw.core.loadBalance;

import java.util.List;

/**
 * @author shichaoyang
 * @Description: 一致性哈希
 * @date 2019-04-04 17:08
 */
public class LoadBalancePerConsistentHash implements LoadBalance {

    @Override
    public String selectService(List<String> services) {

        //服务端列表不能为空
        if (services == null || services.size() == 0) {
            return null;
        }

        int size = services.size();

        //单个元素直接返回
        if (size == 1) {
            return services.get(0);
        }

        //一致性哈希
        Long index = System.currentTimeMillis() % size;
        return services.get(index.intValue());
    }

}
