package com.tw.core.loadBalance;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author shichaoyang
 * @Description: 随机
 * @date 2019-04-04 17:07
 */
public class LoadBalancePerRandom implements LoadBalance {

    @Override
    public String selectService(List<String> services) {
        //服务端列表不能为空
        if (services == null || services.size() == 0) {
            return null;
        }

        int size = services.size();

        //单个元素直接返回
        if (size == 1) {
            return services.get(0);
        }

        //随机获取
        return services.get(ThreadLocalRandom.current().nextInt(size));
    }
}
