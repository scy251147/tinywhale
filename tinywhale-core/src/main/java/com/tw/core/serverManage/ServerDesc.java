package com.tw.core.serverManage;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.x.discovery.ServiceDiscovery;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-06-05 16:30
 */
public class ServerDesc {

    private CuratorFramework zkClient;

    private ServiceDiscovery serviceDiscovery;

    public CuratorFramework getZkClient() {
        return zkClient;
    }

    public void setZkClient(CuratorFramework zkClient) {
        this.zkClient = zkClient;
    }

    public ServiceDiscovery getServiceDiscovery() {
        return serviceDiscovery;
    }

    public void setServiceDiscovery(ServiceDiscovery serviceDiscovery) {
        this.serviceDiscovery = serviceDiscovery;
    }
}
