package com.tw.core.serverManage;

import com.alibaba.fastjson.JSON;
import com.tw.core.TinyWhaleConst;
import com.tw.core.loadBalance.LoadBalance;
import com.tw.core.loadBalance.LoadBalancePerRandom;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.x.discovery.ServiceInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author shichaoyang
 * @Description: 服务发现
 * @date 2019-02-24 14:29
 */
public class ServerDiscovery extends ServerBase {

    /**
     * 日志打印
     */
    private static final Logger logger = LoggerFactory.getLogger(ServerDiscovery.class);

    /**
     * 带参构造
     * @param zkAddress
     */
    public ServerDiscovery(String zkAddress) {
        super(zkAddress);
        serverDesc = open();
        if (serverDesc == null) {
            logger.error("ServerDiscovery.ServerDiscovery，连接zk服务器出错");
            return;
        }
        watchNode(serverDesc.getZkClient());
    }

    /**
     * 负载均衡
     */
    private LoadBalance loadBalance;

    /**
     * 客户端
     */
    private ServerDesc serverDesc;

    /**
     * 服务发现
     * @return
     */
    public String discover(String interfaceName) {
        //如果未设置负载均衡，则默认为随机轮询
        if (loadBalance == null) {
            loadBalance = new LoadBalancePerRandom();
        }
        //如果本地缓存无数据，则拉取服务器套接字到本地缓存
        if (map.size() == 0) {
            logger.error("serverCache为空,开始加载服务器socket到本地缓存...");
            loadServerCache(interfaceName);
        }
        //返回负载均衡结果
        return loadBalance.selectService(map.get(interfaceName));
    }

    /**
     * 监听zk节点改变
     * @param zkClient
     */
    public void watchNode(CuratorFramework zkClient) {

        TreeCache treeCache = null;
        try {
            treeCache = new TreeCache(zkClient, TinyWhaleConst.ZK_REGISTRY_PATH);
            treeCache.start();
        } catch (Exception e) {
            logger.error("ServerDiscovery.watchNode异常", e);
        }

        treeCache.getListenable().addListener((zkClientx, event) -> {

            if(event == null){
                return;
            }

            if(event.getData() == null){
                return;
            }

            if (event.getData().getData() == null || event.getData().getData().length == 0) {
                return;
            }

            ServiceInstance serviceInstance = instanceSerializer.deserialize(event.getData().getData());
            String serverSocket = serviceInstance.getAddress() + ":" + serviceInstance.getPort();
            switch (event.getType()) {
                case NODE_ADDED:
                    addServerCache(serviceInstance.getName(), serverSocket);
                    break;
                case NODE_REMOVED:
                    rmvServerCache(serviceInstance.getName(), serverSocket);
                    break;
                default:
                    break;
            }
            logger.error("当前服务器列表:" + JSON.toJSONString(map));
        });
    }

    /**
     * 设置负载均衡
     * @param loadBalance
     */
    public void setLoadBalance(LoadBalance loadBalance) {
        this.loadBalance = loadBalance;
    }
}
