package com.tw.core.serverManage;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author shichaoyang
 * @Description: 本地缓存
 * @date 2019-06-05 19:20
 */
public interface ServerCache {

    /**
     * 本地共享cache，主要用于加载注册中心中的服务器套接字
     */
    Map<String, List<String>> map = new ConcurrentHashMap<>();

    /**
     * serverCache缓存加载
     * @param interfaceName
     */
    void loadServerCache(String interfaceName);

    /**
     * serverCache缓存新增
     * @param interfaceName
     * @param socket
     */
    void addServerCache(String interfaceName, String socket);

    /**
     * serverCahce缓存移除
     * @param interfaceName
     * @param socket
     */
    void rmvServerCache(String interfaceName, String socket);
}
