package com.tw.core.serverManage;

import org.apache.curator.x.discovery.ServiceInstance;
import org.apache.curator.x.discovery.ServiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import java.util.Set;

/**
 * @author shichaoyang
 * @Description: 服务治理
 * @date 2019-02-21 21:02
 */
public class ServerRegistry extends ServerBase {

    /**
     * 日志打印
     */
    private static final Logger logger = LoggerFactory.getLogger(ServerRegistry.class);

    /**
     * 带参构造
     *
     * @param zkAddress
     */
    public ServerRegistry(String zkAddress) {
        super(zkAddress);
    }

    /**
     * 服务注册
     *
     * @param serverInterfaces
     * @param host
     * @param port
     * @throws Exception
     */
    public void register(Set<String> serverInterfaces, String host, int port) throws Exception {

        if (serverInterfaces == null || serverInterfaces.size() == 0) {
            logger.error("ServerRegistry.register出错，接口不能为空");
            return;
        }

        if (StringUtils.isEmpty(host)) {
            logger.error("ServerRegistry.register出错，服务host不能为空");
            return;
        }

        if (port < 0 || port > 65535) {
            logger.error("ServerRegistry.register出错，服务端口设置错误");
            return;
        }

        ServerDesc serverDesc = open();

        if (serverDesc == null) {
            logger.error("ServerRegistry.register出错，连接zk服务器出错");
            return;
        }

        //进行服务注册
        for (String interfaceName : serverInterfaces) {
            ServiceInstance serviceInstance = ServiceInstance.builder()
                    .name(interfaceName)
                    .address(host)
                    .port(port)
                    .serviceType(ServiceType.DYNAMIC_SEQUENTIAL)
                    .build();
            try {
                serverDesc.getServiceDiscovery().registerService(serviceInstance);
                addServerCache(interfaceName, host + ":" + port);
            } catch (Exception e) {
                logger.error("ServerRegistry.register出错,interfaceName:" + interfaceName, e);
            }
        }
    }
}
