package com.tw.core.serverManage;

import com.tw.core.TinyWhaleConst;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.apache.curator.x.discovery.ServiceDiscoveryBuilder;
import org.apache.curator.x.discovery.ServiceInstance;
import org.apache.curator.x.discovery.details.InstanceSerializer;
import org.apache.curator.x.discovery.details.JsonInstanceSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shichaoyang
 * @Description: 服务治理
 * @date 2019-04-11 17:57
 */
public abstract class ServerBase implements ServerCache {

    private static final Logger logger = LoggerFactory.getLogger(ServerBase.class);

    /**
     * 基类带参构造
     *
     * @param zkAddress
     */
    public ServerBase(String zkAddress) {
        this.zkAddress = zkAddress;
    }

    /**
     * zk地址
     */
    private String zkAddress;

    /**
     * curator操作框架
     */
    private CuratorFramework zkClient;

    /**
     * 服务发现
     */
    private ServiceDiscovery serviceDiscovery;

    /**
     * 序列化与反序列化
     */
    protected InstanceSerializer instanceSerializer = new JsonInstanceSerializer(ServiceInstance.class);

    /**
     * 连接zk服务器
     *
     * @return
     */
    protected ServerDesc open() {
        try {
            //连接zk服务器
            if(zkClient == null) {
                zkClient = CuratorFrameworkFactory.newClient(zkAddress, new RetryNTimes(5, TinyWhaleConst.ZK_SESSION_TIMEOUT));
                zkClient.start();
                zkClient.blockUntilConnected();
            }

            //启用服务发现
            if(serviceDiscovery == null) {
                serviceDiscovery = ServiceDiscoveryBuilder.builder(ServiceInstance.class)
                        .client(zkClient)
                        .basePath(TinyWhaleConst.ZK_REGISTRY_PATH)
                        .watchInstances(true)
                        .serializer(instanceSerializer)
                        .build();
                serviceDiscovery.start();
            }

            //结果返回
            ServerDesc serverDesc = new ServerDesc();
            serverDesc.setZkClient(zkClient);
            serverDesc.setServiceDiscovery(serviceDiscovery);
            return serverDesc;
        } catch (Exception e) {
            logger.error("ServerBase.open出现错误", e);
            return null;
        }
    }

    /**
     * 关闭zk服务器
     */
    protected void close() throws IOException {
        try {
            if (serviceDiscovery != null) {
                CloseableUtils.closeQuietly(serviceDiscovery);
            }
            if (zkClient != null) {
                CloseableUtils.closeQuietly(zkClient);
            }
        } catch (Exception e) {
            if (serviceDiscovery != null) {
                serviceDiscovery.close();
            }
            if (zkClient != null) {
                zkClient.close();
            }
        }
    }

    /**
     * serverCache缓存加载
     * @param interfaceName
     */
    @Override
    public void loadServerCache(String interfaceName) {
        try {
            List<ServiceInstance> serviceInstances = (List<ServiceInstance>) serviceDiscovery.queryForInstances(interfaceName);
            for (ServiceInstance serviceInstance : serviceInstances) {
                String socket = serviceInstance.getAddress() + ":" + serviceInstance.getPort();
                addServerCache(interfaceName, socket);
            }
        } catch (Exception e) {
            logger.error("ServerBase.loadServerCache异常", e);
        }
    }

    /**
     * serverCache缓存新增
     * @param interfaceName
     * @param socket
     */
    @Override
    public void addServerCache(String interfaceName, String socket){
        if (!map.containsKey(interfaceName)) {
            List<String> set = new ArrayList<>();
            set.add(socket);
            map.put(interfaceName, set);
        } else {
            map.get(interfaceName).add(socket);
        }
    }

    /**
     * serverCahce缓存移除
     * @param interfaceName
     * @param socket
     */
    @Override
    public void rmvServerCache(String interfaceName, String socket) {
        if (map.containsKey(interfaceName)) {
            map.remove(interfaceName, socket);
        }
    }
}