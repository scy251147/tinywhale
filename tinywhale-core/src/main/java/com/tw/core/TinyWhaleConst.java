package com.tw.core;

/**
 * @author shichaoyang
 * @Description: 常量
 * @date 2019-02-21 21:17
 */
public interface TinyWhaleConst {

    int ZK_SESSION_TIMEOUT = 5000;

    String ZK_REGISTRY_PATH = "/registry";

    String ZK_DATA_PATH = ZK_REGISTRY_PATH + "/data";

}
