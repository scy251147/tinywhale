package com.tw.message;

/**
 * @author shichaoyang
 * @Description: 登录状态
 * @date 2019-05-30 9:59
 */
public enum LoginType {

    //登录成功
    SUCCESS((byte) 0),
    //登录失败
    FAIL((byte) -1),
    //重复登录
    DUPLICATE((byte) -2);

    private byte value;

    private LoginType(byte value) {
        this.value = value;
    }

    public byte value() {
        return this.value;
    }

}
