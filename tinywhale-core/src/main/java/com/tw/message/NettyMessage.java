package com.tw.message;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-01-30 10:40
 */
public class NettyMessage<T> {

    /**
     * 会话ID
     */
    private long sessionId;

    /**
     * 消息类型
     */
    private byte type;

    /**
     * 消息体
     */
    private T body;


    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Header [sessionId=" + sessionId + ",type=" + type + "]";
    }


}
