package com.tw.codec;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-10 18:26
 */
public class NettyLengthDecoder extends LengthFieldBasedFrameDecoder {
    public NettyLengthDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength);
    }
}
