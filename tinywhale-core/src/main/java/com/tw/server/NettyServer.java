package com.tw.server;

import com.tw.codec.NettyLengthDecoder;
import com.tw.codec.NettyMessageDecoder;
import com.tw.codec.NettyMessageEncoder;
import com.tw.core.serverManage.ServerRegistry;
import com.tw.handler.HeartBeatResponseHandler;
import com.tw.handler.LoginAuthResponseHandler;
import com.tw.handler.ServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-01-30 14:14
 */
public class NettyServer {

    /**
     * 服务端带参构造
     * @param serverAddress
     * @param serviceRegistry
     * @param serverBeans
     */
    public NettyServer(String serverAddress, ServerRegistry serviceRegistry, Map<String, Object> serverBeans) {
        this.serverAddress = serverAddress;
        this.serviceRegistry = serviceRegistry;
        this.serverBeans = serverBeans;
    }

    /**
     * 日志记录
     */
    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);

    /**
     * 服务端绑定地址
     */
    private String serverAddress;

    /**
     * 服务注册
     */
    private ServerRegistry serviceRegistry;

    /**
     * 服务端加载的bean列表
     */
    private Map<String, Object> serverBeans;

    /**
     * 主事件池
     */
    private EventLoopGroup bossGroup = new NioEventLoopGroup();

    /**
     * 副事件池
     */
    private EventLoopGroup workerGroup = new NioEventLoopGroup();

    /**
     * 服务端通道
     */
    private Channel serverChannel;

    /**
     * 绑定本机监听
     *
     * @throws Exception
     */
    public void bind() throws Exception {

        //启动器
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        //为Acceptor设置事件池，为客户端接收设置事件池
        serverBootstrap.group(bossGroup, workerGroup)
                //工厂模式，创建NioServerSocketChannel类对象
                .channel(NioServerSocketChannel.class)
                //等待队列大小
                .option(ChannelOption.SO_BACKLOG, 100)
                //地址复用
                .option(ChannelOption.SO_REUSEADDR, true)
                //开启Nagle算法，
                //网络好的时候：对响应要求比较高的业务，不建议开启，比如玩游戏，键盘数据，鼠标响应等，需要实时呈现；
                //            对响应比较低的业务，建议开启，可以有效减少小数据包传输。
                //网络差的时候：不建议开启，否则会导致整体效果更差。
                .option(ChannelOption.TCP_NODELAY, true)
                //日志记录组件的level
                .handler(new LoggingHandler(LogLevel.INFO))
                //各种业务处理handler
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) throws Exception {
                        //空闲检测handler，用于检测通道空闲状态
                        channel.pipeline().addLast("idleStateHandler", new IdleStateHandler(45, 45, 120));
                        //拆分消息，头部字节长度为4字节
                        channel.pipeline().addLast("nettyLengthDecoder", new NettyLengthDecoder(1024 * 1024, 0, 4));
                        channel.pipeline().addLast("nettyMessageEncoder", new NettyMessageEncoder());
                        channel.pipeline().addLast("nettyMessageDecoder", new NettyMessageDecoder());
                        //心跳包业务处理，一般需要配置idleStateHandler一起使用
                        channel.pipeline().addLast("heartBeatHandler", new HeartBeatResponseHandler());
                        //服务端先进行鉴权，然后处理业务
                        channel.pipeline().addLast("loginAuthResponseHandler", new LoginAuthResponseHandler());
                        //业务处理handler
                        channel.pipeline().addLast("nettyHandler", new ServerHandler(serverBeans));
                    }
                });

        //获取ip和端口
        String[] array = serverAddress.split(":");
        String host = array[0];
        int port = Integer.parseInt(array[1]);

        //绑定端口，同步等待成功
        ChannelFuture future = serverBootstrap.bind(host, port).sync();

        //注册连接事件监听器
        future.addListener(cfl -> {
            if (cfl.isSuccess()) {
                logger.info("服务端[" + host + ":" + port + "]已上线...");
                serverChannel = future.channel();
            }
        });

        //注册关闭事件监听器
        future.channel().closeFuture().addListener(cfl -> {
            //关闭服务端
            close();
            logger.info("服务端[" + host + ":" + port + "]已下线...");
        });

        //注册服务地址
        if (serviceRegistry != null) {
            serviceRegistry.register(serverBeans.keySet(), host, port);
        }
    }

    /**
     * 关闭server
     */
    public void close() {
        //关闭套接字
        if(serverChannel!=null){
            serverChannel.close();
        }
        //关闭主线程组
        if (bossGroup != null) {
            bossGroup.shutdownGracefully();
        }
        //关闭副线程组
        if (workerGroup != null) {
            workerGroup.shutdownGracefully();
        }
    }
}
