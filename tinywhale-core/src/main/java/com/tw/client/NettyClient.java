package com.tw.client;

import com.tw.codec.NettyLengthDecoder;
import com.tw.codec.NettyMessageDecoder;
import com.tw.codec.NettyMessageEncoder;
import com.tw.core.TinyWhaleAsyncCallback;
import com.tw.core.TinyWhaleFuture;
import com.tw.handler.ClientHandler;
import com.tw.handler.HeartBeatRequestHandler;
import com.tw.handler.LoginAuthRequestHandler;
import com.tw.message.NettyMessage;
import com.tw.message.NettyRequest;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-01-30 13:59
 */
public class NettyClient {

    /**
     * 日志记录
     */
    private static final Logger logger = LoggerFactory.getLogger(NettyClient.class);

    /**
     * 客户端请求Future列表
     */
    private Map<String, TinyWhaleFuture> clientFutures = new ConcurrentHashMap<>();


    /**
     * 客户端业务处理handler
     */
    private ClientHandler clientHandler = new ClientHandler(clientFutures);

    /**
     * 事件池
     */
    private EventLoopGroup group = new NioEventLoopGroup();

    /**
     * 启动器
     */
    private Bootstrap bootstrap = new Bootstrap();

    /**
     * 客户端通道
     */
    private Channel clientChannel;

    /**
     * 客户端连接
     * @param host
     * @param port
     * @throws InterruptedException
     */
    public NettyClient(String host, int port) throws InterruptedException {
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) throws Exception {
                        //通道空闲检测
                        channel.pipeline().addLast("idleStateHandler", new IdleStateHandler(45, 45, 120));
                        //拆分消息，头部字节长度为4字节
                        channel.pipeline().addLast("nettyLengthDecoder", new NettyLengthDecoder(1024 * 1024, 0, 4));
                        channel.pipeline().addLast("nettyMessageEncoder", new NettyMessageEncoder());
                        channel.pipeline().addLast("nettyMessageDecoder", new NettyMessageDecoder());
                        //业务处理
                        channel.pipeline().addLast("clientHandler", clientHandler);
                        //鉴权处理
                        channel.pipeline().addLast("loginAuthHandler", new LoginAuthRequestHandler());
                    }
                });

        //发起同步连接操作
        ChannelFuture channelFuture = bootstrap.connect(host, port).sync();

        //连接成功
        if(channelFuture.isDone()){
            logger.info("客户端[" + channelFuture.channel().localAddress().toString() + "]已连接...");
            clientChannel = channelFuture.channel();
        }
        //连接失败，重试
        else{
            logger.info("客户端[" + channelFuture.channel().localAddress().toString() + "]连接失败，重新连接中...");
            channelFuture.channel().close();
            bootstrap.connect(host, port);
        }

        //注册关闭事件
        channelFuture.channel().closeFuture().addListener(cfl -> {
            close();
            logger.info("客户端[" + channelFuture.channel().localAddress().toString() + "]已断开...");
        });
    }

    /**
     * 客户端关闭
     */
    private void close() {
        //关闭客户端套接字
        if(clientChannel!=null){
            clientChannel.close();
        }
        //关闭客户端线程组
        if (group != null) {
            group.shutdownGracefully();
        }
    }

    /**
     * 客户端发送消息，将获取的Future句柄保存到clientFutures列表
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public TinyWhaleFuture send(NettyMessage<NettyRequest> request) {
        TinyWhaleFuture rpcFuture = new TinyWhaleFuture(request);
        rpcFuture.addCallback(new TinyWhaleAsyncCallback() {
            @Override
            public void success(Object result) {
            }

            @Override
            public void fail(Exception e) {
                logger.error("发送失败", e);
            }
        });
        clientFutures.put(request.getBody().getRequestId(), rpcFuture);
        clientHandler.sendMessage(request);
        return rpcFuture;
    }
}


