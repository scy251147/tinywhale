package com.tw.handler;

import com.tw.message.LoginType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import com.tw.message.MessageType;
import com.tw.message.NettyMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-01-30 11:37
 */
public class LoginAuthRequestHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(LoginAuthRequestHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("客户端[" + ctx.channel().localAddress() + "]发起鉴权请求...");
        ctx.writeAndFlush(buildLoginRequest());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        NettyMessage message = (NettyMessage) msg;
        //如果是握手应答消息，需要判断是否认证成功
        if (message != null && message.getType() == MessageType.LOGIN_RESP.value()) {
            byte loginResult = (byte) message.getBody();
            //鉴权成功，保持连接
            if (loginResult == LoginType.SUCCESS.value()) {
                logger.info("客户端[" + ctx.channel().localAddress() + "]鉴权成功，允许通信: " + message);
            }
            //鉴权失败，关闭连接
            else if (loginResult == LoginType.FAIL.value()) {
                logger.info("客户端[" + ctx.channel().localAddress() + "]鉴权失败，连接断开: " + message);
                ctx.close();
            }
            //重复鉴权，保护连接
            else if (loginResult == LoginType.DUPLICATE.value()) {
                logger.info("客户端[" + ctx.channel().localAddress() + "]重复鉴权");
            }
            //无效鉴权，关闭连接
            else {
                logger.info("客户端[" + ctx.channel().localAddress() + "]无效鉴权，连接断开");
                ctx.close();
            }
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    private NettyMessage buildLoginRequest() {
        NettyMessage message = new NettyMessage();
        message.setType(MessageType.LOGIN_REQ.value());
        return message;
    }
}

