package com.tw.handler;

import com.tw.core.TinyWhaleFuture;
import com.tw.message.MessageType;
import com.tw.message.NettyMessage;
import com.tw.message.NettyResponse;
import io.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-03-28 15:44
 */
public class ClientHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ClientHandler.class);

    public ClientHandler(Map<String, TinyWhaleFuture> clientFutures){
        this.clientFutures = clientFutures;
    }

    private Map<String, TinyWhaleFuture> clientFutures;

    private ChannelHandlerContext ctx;
    private ChannelPromise promise;
    private NettyMessage response;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        this.ctx = ctx;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        NettyMessage message = (NettyMessage) msg;
        //如果是业务包
        if (message != null && message.getType() == MessageType.SERVICE_RESP.value()) {
            //解包
            NettyResponse response = (NettyResponse) message.getBody();
            //将操作置为完成状态
            clientFutures.get(response.getRequestId()).done(message);
            //移除完成的操作
            clientFutures.remove(response.getRequestId());
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    public void sendMessage(Object message) {
        while (ctx == null) {
            try {
                TimeUnit.MILLISECONDS.sleep(1);
            } catch (InterruptedException e) {
                logger.error("等待ChannelHandlerContext实例化过程中出错",e);
            }
        }
        ctx.writeAndFlush(message);
    }

    public NettyMessage getResponse(){
        return response;
    }

}