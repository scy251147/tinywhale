package com.tw.handler;

import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import com.tw.message.MessageType;
import com.tw.message.NettyMessage;
import com.tw.message.NettyRequest;
import com.tw.message.NettyResponse;
import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author shichaoyang
 * @Description: 服务端处理rpc请求
 * @date 2019-02-24 12:57
 */
public class ServerHandler  extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ServerHandler.class);

    public ServerHandler(Map<String, Object> ServerBeans){
        this.ServerBeans = ServerBeans;
    }

    private final Map<String, Object> ServerBeans;

    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    /**
     * 服务端接收到客户端的请求
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //开启业务线程池进行处理
        executorService.submit(() -> {
            //接收到的请求进行还原
            NettyMessage message = (NettyMessage) msg;
            //如果此包属于业务处理包
            if (message != null && message.getType() == MessageType.SERVICE_REQ.value()) {
                //设置回应数据包
                NettyMessage<NettyResponse> response = new NettyMessage();
                response.setType(MessageType.SERVICE_RESP.value());
                response.setBody(new NettyResponse());

                NettyMessage<NettyRequest> request = (NettyMessage) msg;
                response.getBody().setRequestId(request.getBody().getRequestId());

                try {
                    //反射调用方法，并将结果放入回应数据包中
                    NettyResponse result = handle(request.getBody());
                    response.setBody(result);
                } catch (Throwable t) {
                    response.getBody().setError(t);
                }
                //将回应数据包推送到缓冲区
                ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
            }
        });
    }

    /**
     * 反射调用方法并得到结果
     * @param request
     * @return
     * @throws Throwable
     */
    private NettyResponse handle(NettyRequest request) throws Throwable {
        //待调用的类名称
        String className = request.getClassName();
        //根据类名称拿到对应的bean
        Object serviceBean = ServerBeans.get(className);
        //得到类
        Class<?> serviceClass = serviceBean.getClass();
        //得到方法
        String methodName = request.getMethodName();
        //得到入参类型
        Class<?>[] parameterTypes = request.getParameterTypes();
        //得到入参值
        Object[] parameters = request.getParameterValues();

        //构建待调用的类对象
        FastClass serviceFastClass = FastClass.create(serviceClass);
        //构建待调用的方法对象
        FastMethod serviceFastMethod = serviceFastClass.getMethod(methodName, parameterTypes);
        //反射调用并得到结果
        Object result = serviceFastMethod.invoke(serviceBean, parameters);
        //将调用结果封装并返回
        NettyResponse response = new NettyResponse();
        response.setResult(result);
        response.setRequestId(request.getRequestId());
        return response;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.error("服务端异常，服务端Socket[" + ctx.channel().localAddress().toString() + "], 客户端Socket[" + ctx.channel().remoteAddress().toString() + "]", cause);
        ctx.close();
    }

}
