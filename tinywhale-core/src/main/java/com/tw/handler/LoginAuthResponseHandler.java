package com.tw.handler;

import com.tw.message.LoginType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import com.tw.message.MessageType;
import com.tw.message.NettyMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-01-30 13:27
 */
public class LoginAuthResponseHandler extends ChannelInboundHandlerAdapter {

    /**
     * 日志打印组件
     */
    private static final Logger logger = LoggerFactory.getLogger(LoginAuthResponseHandler.class);

    /**
     * 已登录客户端列表
     */
    private Map<String, Boolean> clientList = new ConcurrentHashMap<>();

    /**
     * 白名单
     */
    private String[] whiteList = {"127.0.0.1","192.168.1.104"};

    /**
     * 黑名单
     */
    private String[] blackList = {"10.114.12.41"};

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        NettyMessage message = (NettyMessage) msg;

        //属于自己处理的消息，则进行处理
        if (message != null && message.getType() == MessageType.LOGIN_REQ.value()) {
            String clientSocket = ctx.channel().remoteAddress().toString();
            NettyMessage loginResponse;
            //如果之前登录过，则直接回应已登录，无需重复登录
            if (clientList.containsKey(clientSocket)) {
                loginResponse = buildResponse(LoginType.DUPLICATE.value());
            }
            //如果之前未登录过，则放入客户端列表
            else {
                InetSocketAddress address = (InetSocketAddress) ctx.channel().remoteAddress();
                String ip = address.getAddress().getHostAddress();
                boolean isOk = false;
                //白名单校验
                for (String whiteIP : whiteList) {
                    if (whiteIP.equals(ip)) {
                        isOk = true;
                        break;
                    }
                }
                //黑名单校验
                for(String blackIP:blackList) {
                    if (blackIP.equals(ip)) {
                        isOk = false;
                        break;
                    }
                }
                //得到返回结果
                loginResponse = isOk ? buildResponse(LoginType.SUCCESS.value()) : buildResponse(LoginType.FAIL.value());
                if (isOk) {
                    logger.info("用户" + clientSocket + "登录成功");
                    clientList.put(clientSocket, true);
                }
            }
            //压到缓冲区
            ctx.writeAndFlush(loginResponse);
        }
        //不属于自己处理的消息，则透传
        else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        clientList.remove(ctx.channel().remoteAddress().toString());
        ctx.close();
        ctx.fireExceptionCaught(cause);
    }

    private NettyMessage buildResponse(byte result) {
        NettyMessage message = new NettyMessage();
        message.setType(MessageType.LOGIN_RESP.value());
        message.setBody(result);
        return message;
    }

}
