package com.tinywhale.deploy.loadClass;

import java.io.*;
import java.util.HashSet;

/**
 * @author shichaoyang
 * @Description: 基于类的热交换， hotswap
 * @date 2021-03-08 17:23
 */
public class CustomClassLoader extends ClassLoader {

    //需要该类加载器直接加载的类文件的基目录
    private String baseDir;

    //需要由该类加载器直接加载的类名
    private HashSet classSet;

    public CustomClassLoader(String baseDir, String[] classes) throws IOException {
        //TODO 存疑，实际上我这里改成super()也能很好的运行
        super(null);
        this.baseDir = baseDir;
        this.classSet = new HashSet();
        loadClassByMe(classes);
    }

    private void loadClassByMe(String[] classes) throws IOException {
        for (int i = 0; i < classes.length; i++) {
            findClass(classes[i]);
            classSet.add(classes[i]);
        }
    }

    /**
     * 重写findclass方法
     *
     * 在ClassLoader中，loadClass方法先从缓存中找，缓存中没有，会代理给父类查找，如果父类中也找不到，就会调用此用户实现的findClass方法
     *
     * @param name
     * @return
     */
    @Override
    protected Class findClass(String name) {
        Class clazz = null;
        StringBuffer stringBuffer = new StringBuffer(baseDir);
        String className = name.replace('.', File.separatorChar) + ".class";
        stringBuffer.append(File.separator + className);
        File classF = new File(stringBuffer.toString());
        try {
            clazz = instantiateClass(name, new FileInputStream(classF), classF.length());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    private Class instantiateClass(String name, InputStream fin, long len) throws IOException {
        byte[] raw = new byte[(int) len];
        fin.read(raw);
        fin.close();
        return defineClass(name, raw, 0, raw.length);
    }

//    protected Class loadClass(String name, boolean resolve) throws ClassNotFoundException {
//        Class cls;
//        //从classloader缓存中查看之前有没有加载过
//        cls = findLoadedClass(name);
//        if (!this.classSet.contains(name) && cls == null) {
//            //classloader缓存没有，则委托系统加载器加载
//            cls = getSystemClassLoader().loadClass(name);
//        }
//        if (cls == null) {
//            throw new ClassNotFoundException(name);
//        }
//        if (resolve) {
//            resolveClass(cls);
//        }
//        return cls;
//    }

}
