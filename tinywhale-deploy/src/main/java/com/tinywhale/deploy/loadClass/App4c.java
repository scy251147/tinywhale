package com.tinywhale.deploy.loadClass;

import com.tw.client.Foo;
import java.lang.reflect.Method;

/**
 * @author shichaoyang
 * @Description: Foo.class在外部改好名字为Foo.class，然后拷贝到client文件夹，点击覆盖即可
 * @date 2021-03-08 14:20
 */
public class App4c {

    public static void main(String[] args) throws Exception {
        while (true) {
            runBy2();
            Thread.sleep(1000);
        }
    }

    /**
     * ClassLoader用来加载class类文件的,实现类的热替换
     * 注意，需要在swap目录下，一层层建立目录com/tw/client/，然后将Foo.class放进去
     * @throws Exception
     */
    public static void runBy1() throws Exception {
        CustomClassLoader customClassLoader = new CustomClassLoader("swap", new String[]{"com.tw.client.Foo"});
        Class clazz = customClassLoader.loadClass("com.tw.client.Foo");
        Object foo = clazz.newInstance();
        Method method = foo.getClass().getMethod("sayHello", new Class[]{});
        method.invoke(foo, new Object[]{});
    }

    public static void runBy2() throws Exception {

        //利用系统的classloader加载，此classloader为AppClassLoader
        //默认情况下，用户写的classloader如果没有多余的处理逻辑，都是先findloadedclassloader，找不到就利用系统classloader找，所以这里为appclassloader
        SystemClassLoader appClassLoader = new SystemClassLoader();
        Class<?> aClass = appClassLoader.loadClass("com.tw.client.Foo");
        //这里可以转换，因为整个工程中，用户自定义的类都是appclassloader加载的
        Foo foo1 = (Foo) aClass.newInstance();
        foo1.sayHello();
        System.out.println(foo1.getClass().getClassLoader());

        //app4c启动类所在的classloader加载，此classloader为CustomClassLoader
        //由于重写了内部实现，所以为用户自定义的classloader
        CustomClassLoader customClassLoader = new CustomClassLoader("swap", new String[]{"com.tw.client.Foo"});
        Class clazz = customClassLoader.loadClass("com.tw.client.Foo");
        //这里不能强转，因为Foo类是由appclassloader加载的，但是clazz对象却是由CustomClassLoader加载的，这里只能用如下方式：
        // Object foo = clazz.newInstance();
        // Method method = foo.getClass().getMethod("sayHello", new Class[]{});
        // 注意Object foo = clazz.newInstance()这句话，是实现热部署机制的关键
        // 为什么说Foo类是appclassloader加载的呢？ 是因为App4c.java这个类，是由appclassloader加载的，相应的，如果你直接引用Foo对象，那也必然是appclassloader加载的

        //Foo foo2 = (Foo) clazz.newInstance();
        Object foo2 = clazz.newInstance();
        Method method = foo2.getClass().getMethod("sayHello", new Class[]{});
        method.invoke(foo2, new Object[]{});
        System.out.println(foo2.getClass().getClassLoader());


    }

}
