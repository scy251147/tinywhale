package com.tinywhale.deploy.loadJar;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;

/**
 * @author shichaoyang
 * @Description:  热替换， @see https://developer.ibm.com/zh/articles/j-lo-hotswapcls/
 * @date 2021-03-08 15:04
 */
public class App4j {

    public static void main(String... args) throws Exception {
        while (true) {
            loadJarFile();
            Thread.sleep(1000);
        }
    }



    /**
     * URLClassLoader 用来加载class文件
     *
     * 注意，需要在swap目录下，一层层建立目录com/tw/client/，然后将Foo.class放进去(Foo文件在tinywhale-client模块中)
     *
     * 替换此Foo.class文件，也可以实现特替换的功能
     *
     * @throws Exception
     */
    public static void loadClassFile() throws Exception {
        File moduleFile = new File("swap");
        URL moduleURL = moduleFile.toURI().toURL();
        URL[] urls = new URL[] { moduleURL };
        BizClassLoader bizClassLoader = new BizClassLoader(urls);

        Class clazz = bizClassLoader.loadClass("com.tw.client.Foo");
        Object foo = clazz.newInstance();

        Method method = foo.getClass().getMethod("sayHello", new Class[]{});
        method.invoke(foo, new Object[]{});

        bizClassLoader.close();
    }

    /**
     * URLClassLoader 用来加载Jar文件, 直接放在swap目录下即可
     *
     * 动态改变jar中类，可以实现热加载
     *
     * @throws Exception
     */
    public static void loadJarFile() throws Exception {
        File moduleFile = new File("swap\\tinywhale-client-0.0.1-SNAPSHOT-biz.jar");
        URL moduleURL = moduleFile.toURI().toURL();
        URL[] urls = new URL[] { moduleURL };
        BizClassLoader bizClassLoader = new BizClassLoader(urls);

        Class clazz = bizClassLoader.loadClass("com.tw.client.Bar");
        Object foo = clazz.newInstance();

        Method method = foo.getClass().getMethod("sayBar", new Class[]{});
        method.invoke(foo, new Object[]{});

        bizClassLoader.close();
    }


}
