package com.tinywhale.deploy.loadJar;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-11 15:26
 */
public class ClassLoaderFactory {

    private static ClassLoader jdkClassLoader;
    private static ClassLoader internalClasssLoader;
    private static Object locker = new Object();

    public static ClassLoader getInternalClassLoader() {
        if (internalClasssLoader == null) {
            synchronized (locker) {
                if (internalClasssLoader == null) {
                    internalClasssLoader = ClassLoaderFactory.class.getClassLoader();
                }
            }
        }
        return internalClasssLoader;
    }

    public static ClassLoader getJDKClassLoader() {
        if (jdkClassLoader == null) {
            synchronized (locker) {
                if (jdkClassLoader == null) {
                    ClassLoader extClassLoader = ClassLoader.getSystemClassLoader();
                    while (extClassLoader.getParent() != null) {
                        extClassLoader = extClassLoader.getParent();
                    }
                    List<URL> jdkUrls = new ArrayList<URL>();
                    try {
                        String javaHome = System.getProperty("java.home").replace(File.separator + "jre", "");
                        URL[] urls = ((URLClassLoader) ClassLoader.getSystemClassLoader()).getURLs();
                        for (URL url : urls) {
                            if (url.getPath().startsWith(javaHome)) {

                                jdkUrls.add(url);
                            }
                        }
                    } catch (Throwable e) {

                    }

                    jdkClassLoader = new URLClassLoader(jdkUrls.toArray(new URL[0]), extClassLoader);
                }
            }

        }
        return jdkClassLoader;
    }
}
