package com.tinywhale.deploy.loadJar;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-11 15:25
 */
public class BizClassLoader extends URLClassLoader {

    public BizClassLoader(URL[] urls) {
        super(urls);
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {

        Class<?> clazz = null;
        clazz = findLoadedClass(name);
        if (clazz != null) {
            return clazz;
        }
        try {
            ClassLoader jdkClassLoader = ClassLoaderFactory.getJDKClassLoader();
            clazz = jdkClassLoader.loadClass(name);
            if (clazz != null) {
                return clazz;
            }
        } catch (ClassNotFoundException e) {

        }
        try {
            clazz = this.findClass(name);
        } catch (ClassNotFoundException e) {
            // TODO: handle exception
        }
        if (clazz == null) {
            clazz = ClassLoaderFactory.getInternalClassLoader().loadClass(name);
        }
        return clazz;

    }

    @Override
    protected Class<?> findClass(final String name) throws ClassNotFoundException {
        return super.findClass(name);
    }

}
