package com.tinywhale.deploy.spi;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-16 14:19
 */
public class NameServiceProvider implements HelloService{

    @Override
    public void sayHello(String name) {
        System.out.println("Hi, your name is " + name);
    }
}
