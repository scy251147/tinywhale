package com.tinywhale.deploy.spi;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-16 14:16
 */
public class HelloServiceProvider implements HelloService {

    @Override
    public void sayHello(String name) {
        System.out.println("Hello " + name);
    }
}
