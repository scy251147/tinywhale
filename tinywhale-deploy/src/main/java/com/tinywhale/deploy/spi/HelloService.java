package com.tinywhale.deploy.spi;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-16 14:12
 */
public interface HelloService {

    void sayHello(String name);

}
