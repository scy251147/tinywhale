package com.tinywhale.deploy.spi;

import java.util.ServiceLoader;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-16 14:13
 */
public class App4s {


    /**
     *
     * 1. 创建HelloService接口
     *
     * 2. 针对1中的接口，创建provider实现
     *
     * 3. 针对2中的实现，在resouces中建立META-INF/services文件夹，在其下建立com.tinywhale.deploy.spi.HelloService文件
     *
     * 4. 将2中的实现路径名+类名放进去，我这里是：
     *    com.tinywhale.deploy.spi.HelloServiceProvider
     *    com.tinywhale.deploy.spi.NameServiceProvider
     *
     * 5. main中利用ServiceLoader遍历执行即可
     *
     * @param args
     * @throws Exception
     */
    public static void main(String...args) throws Exception {
        runBy1();
    }

    private static void runBy1(){
        ServiceLoader<HelloService> serviceLoader = ServiceLoader.load(HelloService.class);
        for (HelloService helloWorldService : serviceLoader) {
            helloWorldService.sayHello("myname");
        }
    }
}
