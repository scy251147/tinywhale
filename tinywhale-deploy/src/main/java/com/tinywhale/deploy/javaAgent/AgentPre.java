package com.tinywhale.deploy.javaAgent;

import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;
import java.lang.instrument.Instrumentation;
import java.util.List;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-06-02 15:59
 */
public class AgentPre {

    public static void premain(String agentArgs, Instrumentation inst) {
        System.out.println("execute premain method");
        //获取当前系统中vm数量
        List<VirtualMachineDescriptor> list = VirtualMachine.list();
        ThreadHelper.threadLocal.set(list);
    }

}
