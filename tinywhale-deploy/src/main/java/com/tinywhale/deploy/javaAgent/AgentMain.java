package com.tinywhale.deploy.javaAgent;

import java.lang.instrument.Instrumentation;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-06-02 17:56
 */
public class AgentMain {

    public static void agentmain(String agentArgs, Instrumentation inst) {
        System.out.println("execute agentmain method");
    }

}
