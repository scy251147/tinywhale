package com.tinywhale.deploy.javaAgent;

import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;
import java.io.File;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-06-02 15:56
 */
public class App4a {

    public static void main(String... args) throws Exception {
        System.out.println("execute main method ");
        attach();
    }

    private static void attach() {
        File agentFile = Paths.get("D:\\app\\tinywhale\\tinywhale-deploy\\target\\tinywhale-deploy-1.0-SNAPSHOT.jar").toFile();
        try {
            VirtualMachine jvm = VirtualMachine.attach(getPid());
            jvm.loadAgent(agentFile.getAbsolutePath());
            //jvm.detach();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static String getPid() {
        //启动前vm数量
        List<VirtualMachineDescriptor> originlList = (List<VirtualMachineDescriptor>) ThreadHelper.threadLocal.get();
        //启动后vm数量
        List<VirtualMachineDescriptor> currentList = VirtualMachine.list();
        //差值即为当前启动的vm
        currentList.removeAll(originlList);
        //返回pid
        return currentList.get(0).id()+"";
    }
}
