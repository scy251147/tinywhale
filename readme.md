```
# JMH version: 1.19
# VM version: JDK 1.8.0_162, VM 25.162-b12
# VM invoker: C:\Program Files\Java\jdk1.8.0_162\jre\bin\java.exe
# VM options: -Dvisualvm.id=2173294620360352 -javaagent:D:\soft\IntelliJ IDEA 2017.2.4\lib\idea_rt.jar=64948:D:\soft\IntelliJ IDEA 2017.2.4\bin -Dfile.encoding=UTF-8
# Warmup: 20 iterations, 1 s each
# Measurement: 20 iterations, 1 s each
# Timeout: 10 min per iteration
# Threads: 4 threads, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: com.tw.PerformanceBenchmark.test

# Run progress: 0.00% complete, ETA 00:00:40
# Fork: 1 of 1
# Warmup Iteration   1: log4j:WARN No appenders could be found for logger (org.apache.zookeeper.ZooKeeper).
log4j:WARN Please initialize the log4j system properly.
204.648 ops/s
# Warmup Iteration   2: 121.743 ops/s
# Warmup Iteration   3: 103.496 ops/s
# Warmup Iteration   4: 125.844 ops/s
# Warmup Iteration   5: 179.424 ops/s
# Warmup Iteration   6: 120.635 ops/s
# Warmup Iteration   7: 86.545 ops/s
# Warmup Iteration   8: 184.926 ops/s
# Warmup Iteration   9: 136.450 ops/s
# Warmup Iteration  10: 169.146 ops/s
# Warmup Iteration  11: 161.570 ops/s
# Warmup Iteration  12: 157.327 ops/s
# Warmup Iteration  13: 137.892 ops/s
# Warmup Iteration  14: 120.632 ops/s
# Warmup Iteration  15: 191.016 ops/s
# Warmup Iteration  16: 183.634 ops/s
# Warmup Iteration  17: 97.625 ops/s
# Warmup Iteration  18: 145.066 ops/s
# Warmup Iteration  19: 203.741 ops/s
# Warmup Iteration  20: 111.741 ops/s
Iteration   1: 156.113 ops/s
Iteration   2: 203.618 ops/s
Iteration   3: 134.177 ops/s
Iteration   4: 121.404 ops/s
Iteration   5: 197.915 ops/s
Iteration   6: 199.298 ops/s
Iteration   7: 65.825 ops/s
Iteration   8: 184.359 ops/s
Iteration   9: 212.469 ops/s
Iteration  10: 139.439 ops/s
Iteration  11: 112.174 ops/s
Iteration  12: 199.004 ops/s
Iteration  13: 195.232 ops/s
Iteration  14: 90.157 ops/s
Iteration  15: 115.724 ops/s
Iteration  16: 191.738 ops/s
Iteration  17: 213.633 ops/s
Iteration  18: 159.058 ops/s
Iteration  19: 62.399 ops/s
Iteration  20: 165.608 ops/s


Result "com.tw.PerformanceBenchmark.test":
  155.967 ±(99.9%) 42.118 ops/s [Average]
  (min, avg, max) = (62.399, 155.967, 213.633), stdev = 48.503
  CI (99.9%): [113.849, 198.085] (assumes normal distribution)


# Run complete. Total time: 00:00:50

Benchmark             Mode  Cnt    Score    Error  Units
ProxyBenchmark.test  thrpt   20  155.967 ± 42.118  ops/s
```