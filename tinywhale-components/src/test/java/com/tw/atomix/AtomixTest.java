package com.tw.atomix;

import com.tw.AbstractTest;
import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;
import io.atomix.core.map.AtomicMap;
import io.atomix.primitive.partition.ManagedPartitionGroup;
import io.atomix.primitive.partition.MemberGroupStrategy;
import io.atomix.protocols.backup.partition.PrimaryBackupPartitionGroup;
import io.atomix.protocols.raft.partition.RaftPartitionGroup;
import io.atomix.utils.time.Versioned;
import org.junit.Test;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.time.Duration;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-30 9:51
 */
public class AtomixTest extends AbstractTest {

    @Test
    public void test() throws IOException {
        AtomixBuilder builder = Atomix.builder();
        builder.withClusterId("cluster1")
                .withAddress("127.0.0.1")
                .build();

        System.in.read();
    }

    @Test
    public void testClient() throws IOException {
        Atomix atomix = Atomix.builder()
                .withMemberId("client1")
                .withAddress("10.192.19.180:6000")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("member1")
                                        .withAddress("10.192.19.181:5000")
                                        .build(),
                                Node.builder()
                                        .withId("member2")
                                        .withAddress("10.192.19.182:5000")
                                        .build(),
                                Node.builder()
                                        .withId("member3")
                                        .withAddress("10.192.19.183:5000")
                                        .build())
                        .build())
                .addPartitionGroup(PrimaryBackupPartitionGroup.builder("data")
                        .withNumPartitions(32)
                        .withMemberGroupStrategy(MemberGroupStrategy.RACK_AWARE)
                        .build()
                ).build();

        atomix.start().join();

        System.in.read();
    }

    @Test
    public void test1() throws IOException {
        Atomix atomix = Atomix.builder()
                .withMemberId("member0")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("member1")
                                        .withAddress("10.192.19.181:5679")
                                        .build(),
                                Node.builder()
                                        .withId("member2")
                                        .withAddress("10.192.19.182:5679")
                                        .build(),
                                Node.builder()
                                        .withId("member3")
                                        .withAddress("10.192.19.183:5679")
                                        .build())
                        .build())
                .withManagementGroup(RaftPartitionGroup.builder("system")
                        .withNumPartitions(1)
                        .withMembers("member1", "member2", "member3")
                        .build())
                .withPartitionGroups(RaftPartitionGroup.builder("raft")
                        .withPartitionSize(3)
                        .withNumPartitions(3)
                        .withMembers("member1", "member2", "member3")
                        .build())
                .build();

        atomix.start().join();

        System.in.read();
    }

    @Test
    public void test2() throws Exception {

        Atomix atomix = new Atomix("src/main/resources/atomix.conf");
        atomix.start().join();

        System.in.read();
    }

}
