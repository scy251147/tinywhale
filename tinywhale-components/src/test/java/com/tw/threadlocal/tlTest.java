package com.tw.threadlocal;

import com.alibaba.fastjson.JSON;
import com.tw.AbstractTest;
import com.tw.components.threadlocal.ContextContainer;
import com.tw.components.threadlocal.ContextParam;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-17 16:11
 */
public class tlTest extends AbstractTest {

    @Test
    public void test1() {
        ContextContainer contextContainer = new ContextContainer.Builder()
                .setAutoRemove(true)
                .build();


        contextContainer.set("shichaoyang");

        ContextContainer contextContainer1 = new ContextContainer.Builder()
                .setAutoRemove(true)
                .build();

        System.out.println(contextContainer1.get());

    }

    @Test
    public void test2() throws InterruptedException {
        Thread t1 = new Thread(()->init());

        Thread t2 = new Thread(()->init());

        t1.join();
        t2.join();

        ContextContainer contextContainer = new ContextContainer.Builder()
                .setContainerName("aaaaa")
                .setAutoRemove(false)
                .build();

        System.out.println(contextContainer.get().toString());
    }

    private void init(){
        ContextContainer contextContainer = new ContextContainer.Builder()
                .setContainerName("aaaaa")
                .setAutoRemove(false)
                .build();

        ContextParam<String> contextParam = new ContextParam<>();
        contextParam.setParams("shichaoyang");

        contextContainer.set(contextParam);
        System.out.println(contextContainer.get());
        System.out.println(contextContainer.get());


    }

    @Test
    public void test3(){
        Boolean x = null;
        if(x!=null &&x){
            System.out.println("true");
        }else{
            System.out.println("false");
        }
    }

    @Test
    public void test4(){
        List<LenScore> lenScores = new ArrayList<>();

        LenScore lenScore2 = new LenScore();
        lenScore2.min = "5000";
        lenScore2.max = "10000";
        lenScores.add(lenScore2);


        LenScore lenScore1 = new LenScore();
        lenScore1.min = "10000";
        lenScore1.max = "20000";
        lenScores.add(lenScore1);


        LenScore lenScore3 = new LenScore();
        lenScore3.min = "20000";
        lenScore3.max = "分及以上";
        lenScores.add(lenScore3);
        System.out.println(transUserScore(lenScores));
    }

    private String transUserScore(List<LenScore> list) {
        String imin = "", imax = "", strLen = "", result = "";
        TreeMap<Integer, String> map = new TreeMap<Integer, String>();
        for (LenScore score : list) {
            if (!imax.equals(score.min)) {
                imax = score.max;
                imin = score.min;
                String middleStr = "分及以上".equals(imax) ? "" : "-";
                String endStr = "分及以上".equals(imax) ? "" : "分、";

                strLen = imin + middleStr + imax + endStr;
                map.put(Integer.parseInt(imin), strLen);
            } else {
                imax = score.max;
                String middleStr = "分及以上".equals(imax) ? "" : "-";
                String endStr = "分及以上".equals(imax) ? "" : "分、";
                strLen = imin + middleStr + imax + endStr;
                map.put(Integer.parseInt(imin), strLen);
            }
        }
        StringBuilder sb = new StringBuilder();
        for (String val : map.values()) {
            sb.append(val);
        }
        if (sb.length() > 0) {
            String endSb = sb.substring(sb.length() - 1, sb.length());
            if ("、".equals(endSb)) {
                result = sb.substring(0, sb.length() - 1);
            } else {
                result = sb.toString();
            }
        }
        return result;
    }

    static class LenScore {
        String min;
        String max;
    }
}


