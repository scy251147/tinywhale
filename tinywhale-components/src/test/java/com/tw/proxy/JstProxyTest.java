package com.tw.proxy;

import com.tw.AbstractTest;
import com.tw.client.Foo;
import com.tw.proxy.biz.Hello;
import javassist.*;
import javassist.bytecode.ClassFile;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * @author shichaoyang
 * @Description: javassist
 * @date 2021-04-07 16:47
 */
public class JstProxyTest extends AbstractTest {

    /**
     * 创建新类 并 写到文件中
     * 此类处于被冻结状态，任何后续更改都不被允许了，因为jvm已经加载
     * 需要解冻后方可继续对此类进行修改
     * @throws Exception
     */
    @Test
    public void test1() throws Exception {
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.makeClass("Point1");
        ctClass.writeFile("biz/");
    }

    /**
     * 新类解冻并修改
     * @throws Exception
     */
    @Test
    public void test2() throws Exception {
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.makeClass("Point2");
        //Point2类写入文件后，会被冻结
        ctClass.writeFile("biz/");

        //Point2类解冻
        ctClass.defrost();
        CtClass ctClass1 = classPool.makeClass("Point2Super");
        ctClass.setSuperclass(ctClass1);

        //这里可以看到Point2类被成功设置了父类
        ctClass.writeFile("biz/");
    }

    /**
     * 阻止类被精简
     */
    @Test
    public void test3(){
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.makeClass("Point3");
        //阻止精简
        ctClass.stopPruning(true);
        ctClass.debugWriteFile("biz/");
    }

    /**
     * 为classloader设置类搜索路径
     * 确认路径下存在Point3类
     */
    @Test
    public void test4() throws Exception {
        ClassPool classPool = ClassPool.getDefault();
        classPool.insertClassPath("biz");
        CtClass ctClass = classPool.get("Point3");
    }

    /**
     * 移除classpool中的对象，释放内存
     */
    @Test
    public void test5() throws Exception {
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.makeClass("Point5");
        ctClass.writeFile("biz/");
        ctClass.detach();
        CtClass ctClass1 = classPool.get("Point5");
    }

    /**
     * 级联classpools设置
     * 如果child.childFirstLookup = true, 子ClassPool将会首先查找自己的目录，然后查找父ClassPool
     * @throws NotFoundException
     */
    @Test
    public void test6() throws NotFoundException {
        //父classpool
        ClassPool parent = ClassPool.getDefault();
        //子classpool
        ClassPool child = new ClassPool(parent);
        child.insertClassPath("biz");
        //附加系统路径
        child.appendSystemPath();
        //修改子classpool搜索行为，将优先从子classpool查找
        child.childFirstLookup = true;
    }

    /**
     * 将classpool中的类重命名
     * 需要注意的是setName()方法改变ClassPool对象中的标记。从可扩展性来看，ClassPool对象是HashTable的合集，setName()方法只是改变了key和Ctclass对象的关联。
     * 因此，对于get("Point")方法之后的所有调用，将不会返回CtClasss对象。ClassPool对象再次读取Point.class的时候，将会创建一个新的CtClass，这是因为和Point关联的CtClass对象已经不存在了
     */
    @Test
    public void test7() throws Exception {
        ClassPool pool = ClassPool.getDefault();
        //注意一定要设置类搜索路径
        pool.insertClassPath("biz");

        CtClass ctClass = pool.makeClass("Point7");
        ctClass.writeFile("biz/");

        CtClass cc = pool.get("Point7");
        CtClass cc1 = pool.get("Point7");//cc1和cc是一致的
        Assert.assertTrue(cc == cc1);

        cc.defrost();
        cc.setName("Pair7");
        CtClass cc2 = pool.get("Pair7"); //cc2和cc是一致的
        Assert.assertTrue(cc2 == cc);
        CtClass cc3 = pool.get("Point7");//cc3和cc是不一致的，因为原来的point7已经没了，直接返回了一个新建的
        Assert.assertFalse(cc3 == cc);
    }

    /**
     * 正确修改类名的方法
     *
     * 如果getAndRename()方法被调用，那么ClassPool首先会基于Point.class来创建一个新的CtClass对象。之后，在CtClass对象被放到HashTable前，它将CtClass对象名称从Point修改为Pair。
     * 因此，getAndRename()方法可以在writeFile()方法或者toBytecode()方法执行后去修改CtClass对象。
     * @throws Exception
     */
    @Test
    public void test8() throws Exception {
        ClassPool pool = ClassPool.getDefault();
        //需要将biz文件夹加入到类搜索路径
        pool.insertClassPath("biz");
        CtClass cc = pool.makeClass("Point8");
        cc.writeFile("biz");
        //错误做法：直接setname，会抛错，因为writefile后，类被冻结了
        //cc.setName("Pair");
        //正确做法：利用getAndRename, 首先创建一个Point8类，然后放到pool前，将名称改为Pair8类，所以cc2和cc是不一样的对象
        CtClass cc2 = pool.getAndRename("Point8", "Pair8");
        Assert.assertFalse(cc2 == cc);
    }

    /**
     * 修改已有的Hello类
     * class Hello{
          public void say(){
            System.out.println("Hello");
          }
        }
     * @throws Exception
     */
    @Test
    public void test9() throws Exception{
        ClassPool pool = ClassPool.getDefault();
        pool.insertClassPath("./com/tw/proxy/biz/");

        Loader cl = new Loader(pool);
        cl.loadClass("Hello");

        CtClass cc = pool.get("Hello");
        CtMethod cm = cc.getDeclaredMethod("say");
        cm.insertBefore("{System.out.println(\"Hello.say():\");}");
        Class clazz = cc.toClass();
        Hello hello1 = (Hello) clazz.newInstance();
        hello1.say();
    }

    @Test
    public void test10() throws Exception {
        ClassPool pool = ClassPool.getDefault();

        //方法一
        ClassClassPath classClassPath = new ClassClassPath(Foo.class);
        System.out.println(classClassPath);
        pool.insertClassPath(classClassPath);

        //方法二
        //pool.appendClassPath("D:/app/tinywhale/tinywhale-client/target/classes/com/tw/client/");

        CtClass ctClass = pool.get("com.tw.client.Foo");
        ClassFile classFile = ctClass.getClassFile();
        Set<String> classNames = classFile.getConstPool().getClassNames();

        boolean flag = false;
        for (String c : classNames) {
            System.out.println(c);
            if (c.contains("java/lang/Thread")) {
                flag = true;
            }
        }

        System.out.print(flag ? "含有子线程" : "不含有子线程");
    }
}
