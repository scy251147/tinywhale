package com.tw.proxy.biz;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-17 17:43
 */
public interface ScyService {

    boolean start();

    boolean stop();
}
