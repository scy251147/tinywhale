package com.tw.proxy;

import com.tw.AbstractTest;
import com.tw.client.HelloService;
import com.tw.components.proxy.JdkProxy;
import org.junit.Before;
import org.junit.Test;
import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-17 17:42
 */
public class JdkProxyTest extends AbstractTest{

    @Resource
    private JdkProxy jdkProxy;

    @Before
    public void after() throws InterruptedException {
        jdkProxy.init();
    }

    @Test
    public void test() throws IOException {
        HelloService helloService = jdkProxy.create(HelloService.class);
        String rst = helloService.hello("get the timestamp");
        System.out.println(rst);
        System.in.read();
    }

}
