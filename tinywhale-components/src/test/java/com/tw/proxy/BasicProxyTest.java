package com.tw.proxy;

import com.alibaba.fastjson.JSON;
import com.tw.AbstractTest;
import com.tw.client.HelloService;
import com.tw.proxy.biz.ScyService;
import org.junit.Test;
import java.lang.reflect.Proxy;
import java.util.UUID;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-17 16:11
 */
public class BasicProxyTest extends AbstractTest {

    @Test
    public void baseTest() {
        HelloService helloService = (HelloService) Proxy.newProxyInstance(HelloService.class.getClassLoader()
                , new Class<?>[]{HelloService.class}
                , (proxy, method, args) -> {
                   return "Hello world";
                });
        String rst = helloService.hello("xxxx");
        System.out.println(rst);
    }

    @Test
    public void baseTest1() {
        ScyService helloService = (ScyService) Proxy.newProxyInstance(ScyService.class.getClassLoader()
                , new Class<?>[]{ScyService.class}
                , (proxy, method, args) -> {
                    String requestId = UUID.randomUUID().toString();
                    String className = method.getDeclaringClass().getName();
                    String methodName = method.getName();
                    Class<?>[] paramTypes = method.getParameterTypes();
                    Object[] paramArgs = args;
                    System.out.println(requestId);
                    System.out.println(className);
                    System.out.println(methodName);
                    System.out.println(JSON.toJSONString(paramTypes));
                    System.out.println(JSON.toJSONString(paramArgs));
                    return false;
                });
        //这里调用的方法，可反馈到内部的method变量上去
        boolean rst = helloService.stop();
        boolean rst1 = helloService.start();
        System.out.println(rst);
        System.out.println(rst1);
    }
}
