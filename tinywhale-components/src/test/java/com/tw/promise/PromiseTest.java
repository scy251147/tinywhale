package com.tw.promise;

import com.tw.AbstractTest;
import com.tw.components.micro.domain.MicroMessage;
import io.netty.util.concurrent.DefaultEventExecutor;
import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.Promise;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.ExecutionException;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-28 17:32
 */
public class PromiseTest extends AbstractTest {

    @Test
    public void test() throws ExecutionException, InterruptedException {
        Promise<MicroMessage> promise = new DefaultPromise<>(new DefaultEventExecutor());
        promise.setFailure(new Throwable("sdlkfjslkdf"));
        System.out.println("isDone:" + promise.isDone());
        System.out.println("isSuccess:"+promise.isSuccess());
        Assert.assertTrue(promise.isDone());
        Assert.assertFalse(promise.isSuccess());

        System.out.println(promise.get());
    }

    @Test
    public void test1() throws ExecutionException, InterruptedException {
        Promise<MicroMessage> promise = new DefaultPromise<>(new DefaultEventExecutor());
        promise.setSuccess(new MicroMessage());
        System.out.println("isDone:" + promise.isDone());
        System.out.println("isSuccess:"+promise.isSuccess());
        Assert.assertTrue(promise.isDone());
        Assert.assertTrue(promise.isSuccess());

        System.out.println(promise.get());
    }

}
