package com.tw.disruptor;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.WorkHandler;
import com.lmax.disruptor.dsl.Disruptor;
import com.tw.AbstractTest;
import com.tw.components.micro.disruptor.MicroFactory;
import com.tw.components.micro.disruptor.MicroProducer;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.PublishMessage;
import org.junit.Test;
import java.io.IOException;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-28 17:49
 */
public class DisruptorTest  extends AbstractTest {

    @Test
    public void test() throws IOException {

        MicroFactory microFactory = new MicroFactory();
        Disruptor<PublishMessage> disruptor = new Disruptor<>(microFactory, 1024, r -> {
            return new Thread(r);
        });

        //注册消费端
        MicroConsumer1 microConsumer1 = new MicroConsumer1();
        MicroConsumer2 microConsumer2 = new MicroConsumer2();
        MicroConsumer3 microConsumer3 = new MicroConsumer3();
        disruptor.handleEventsWithWorkerPool(microConsumer1, microConsumer2,microConsumer3);
        disruptor.start();

        //构建生产端
        RingBuffer<PublishMessage> ringBuffer = disruptor.getRingBuffer();
        MicroProducer microProducer = new MicroProducer(ringBuffer);

        for (int i = 0; i < 10; i++) {
            PublishMessage publishMessage = new PublishMessage();
            publishMessage.setCtx(null);
            MicroMessage microMessage = MicroMessage.buildCommandMessage("command message" + i);
            publishMessage.setMicroMessage(microMessage);

            microProducer.publish(publishMessage);
        }

        System.in.read();
    }

    /**
     * 从测试结果可以看出，消费端消息是分broker发送的，十条消息，也许consumer1接收的是0 2 6，consumer2接收的是1,3,5,7，则consumer3接收的是4,8,9,10
     */

}

class MicroConsumer1 implements WorkHandler<PublishMessage> {

    @Override
    public void onEvent(PublishMessage publishMessage) throws Exception {
        System.out.println("consumer1:"+publishMessage.getMicroMessage().getMessage());
    }
}

class MicroConsumer2 implements WorkHandler<PublishMessage> {

    @Override
    public void onEvent(PublishMessage publishMessage) throws Exception {
        System.out.println("consumer2:"+publishMessage.getMicroMessage().getMessage());
    }
}

class MicroConsumer3 implements WorkHandler<PublishMessage> {

    @Override
    public void onEvent(PublishMessage publishMessage) throws Exception {
        System.out.println("consumer3:"+publishMessage.getMicroMessage().getMessage());
    }
}
