package com.tw.micro;

import com.tw.components.micro.MicroClient;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import io.netty.util.concurrent.Promise;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-03-29 18:39
 */
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Thread)
public class ProxyBenchmark {

    private MicroClient microClient;

    @Setup
    public void init() throws Exception {
        microClient = new MicroClient();
        microClient.init();
        microClient.Connect("127.0.0.1",27017);
    }

    /**
     * benchmark result：
     *  4376217.454 ±(99.9%) 934126.150 ops/s [Average]
     * @return
     * @throws InterruptedException
     */
    @Benchmark
    @GroupThreads(2)
    public String testLongConnection() throws InterruptedException {
        String message = "message timestamp " + System.currentTimeMillis();
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setMessage(message);
        microMessage.setType(MicroType.COMMAND);
        microClient.send(microMessage);
        return "ok";
    }

    /**
     * benchmark result:
     *  424.613 ±(99.9%) 163.990 ops/s [Average]
     * @return
     * @throws InterruptedException
     */
    @Benchmark
    @GroupThreads(2)
    public String testShortConnection() throws InterruptedException {
        microClient.Connect("127.0.0.1",27017);
        String message = "message timestamp " + System.currentTimeMillis();
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setMessage(message);
        microMessage.setType(MicroType.COMMAND);
        microClient.send(microMessage);
        microClient.close();
        return "ok";
    }

    /**
     * benchmark result:
     *  2631.805 ±(99.9%) 110.397 ops/s [Average]
     * @return
     * @throws Exception
     */
    @Benchmark
    @GroupThreads(2)
    public String testSyncSend() throws Exception {
        String message = "message timestamp " + System.currentTimeMillis();
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setMessage(message);
        microMessage.setType(MicroType.COMMAND);
        MicroMessage rst = microClient.syncSend(microMessage);
        return rst.getMessage();
    }

    /**
     * benchmark result:
     * TODO
     * @return
     * @throws Exception
     */
    @Benchmark
    @GroupThreads(2)
    public String testAsyncSend() throws Exception {
        String message = "message timestamp " + System.currentTimeMillis();
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setMessage(message);
        microMessage.setType(MicroType.COMMAND);
        Promise<MicroMessage> promise = microClient.asyncSend(microMessage);
        return "ok";
    }

    /**
     * 从结果可以看出，保持长连接，性能最好；频繁的创建连接，性能最差
     * @param args
     * @throws RunnerException
     */
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(ProxyBenchmark.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(opt).run();
    }
}