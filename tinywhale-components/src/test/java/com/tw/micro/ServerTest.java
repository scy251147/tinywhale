package com.tw.micro;

import com.tw.AbstractTest;
import com.tw.components.micro.MicroServer;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-10 10:23
 */
public class ServerTest extends AbstractTest {

    @Resource
    private MicroServer microServer;

    @Value("${components.server.host}")
    private String host;

    @Value("${components.server.port}")
    private int port;

    @Test
    public void testServer() throws Exception {
        microServer.Start(host, port);
        System.in.read();
    }
}
