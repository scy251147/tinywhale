package com.tw.micro;

import com.tw.AbstractTest;
import com.tw.components.micro.MicroClient;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import io.netty.util.concurrent.Promise;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author shichaoyang
 * @Description: client与服务器之间长连接测试
 * @date 2020-06-10 10:23
 */
public class ClientLongConnectTest extends AbstractTest {

    @Resource
    private MicroClient microClient;

    @Value("${components.server.host}")
    private String host;

    @Value("${components.server.port}")
    private int port;

    @Before
    public void init() throws InterruptedException {
        microClient.Connect(host,port);
    }

    @Test
    public void sendBasic() throws Exception {
        String message = "message ";
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setMessage(message);
        microMessage.setType(MicroType.COMMAND);
        boolean success = microClient.send(microMessage);
        if (!success) {
            Thread.sleep(1000);
            success = microClient.send(microMessage);
        }
        //System.out.println("send client message : " + message);
       // microClient.close();
        System.in.read();
    }

    @Test
    public void sendAsync() throws Exception {
        String message = "message ";
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setMessage(message);
        microMessage.setType(MicroType.COMMAND);
        Promise<MicroMessage> rst = microClient.asyncSend(microMessage);
        System.in.read();
    }

    @Test
    public void sendWithoutAck() throws Exception {
        for (int i = 0; i < 100000; i++) {
            String message = "message index " + i;
            MicroMessage microMessage = new MicroMessage();
            microMessage.setRequestId(UUID.randomUUID().toString());
            microMessage.setMessage(message);
            microMessage.setType(MicroType.COMMAND);
            boolean success = microClient.send(microMessage);
            if(!success){
                Thread.sleep(1000);
                success = microClient.send(microMessage);
            }
            //System.out.println("send client message : " + message);
        }
        //microClient.close();
        System.in.read();
    }

    @Test
    public void asyncSend() throws Exception {
        for (int i = 0; i < 100000; i++) {
            String message = "message index " + i;
            MicroMessage microMessage = new MicroMessage();
            microMessage.setRequestId(UUID.randomUUID().toString());
            microMessage.setMessage(message);
            microMessage.setType(MicroType.COMMAND);
            Promise<MicroMessage> promise = microClient.asyncSend(microMessage).addListener(future -> {
                if(!future.isSuccess()){
                    microClient.asyncSend(microMessage);
                }
            });
            //MicroMessage rst = promise.get(3, TimeUnit.SECONDS);
            //System.out.println("receive message : " + JSON.toJSONString(promise));
        }
        System.in.read();
    }

    @Test
    public void syncSend() throws Exception {
        for (int i = 0; i < 100000; i++) {
            String message = "message index " + i;
            MicroMessage microMessage = new MicroMessage();
            microMessage.setRequestId(UUID.randomUUID().toString());
            microMessage.setMessage(message);
            microMessage.setType(MicroType.COMMAND);
            MicroMessage rst = microClient.syncSend(microMessage);
            if (rst == null) {
                rst = microClient.syncSend(microMessage);
            }
            //System.out.println("get message : " + (rst == null ? "null" : rst.getMessage()));
        }
        System.in.read();
    }


}
