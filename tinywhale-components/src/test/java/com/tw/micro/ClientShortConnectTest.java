package com.tw.micro;

import com.tw.AbstractTest;
import com.tw.components.micro.MicroClient;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author shichaoyang
 * @Description: client与server短连接测试
 * @date 2020-06-16 17:04
 */
public class ClientShortConnectTest extends AbstractTest {

    @Resource
    private MicroClient microClient;

    @Value("${components.server.host}")
    private String host;

    @Value("${components.server.port}")
    private int port;

    @Test
    public void testClient() throws Exception {
        for (int i = 0; i < 10000; i++) {
            microClient.Connect(host, port);
            String message = "message timestamp " + System.currentTimeMillis() + " " + i;
            MicroMessage microMessage = new MicroMessage();
            microMessage.setRequestId(UUID.randomUUID().toString());
            microMessage.setMessage(message);
            microMessage.setType(MicroType.COMMAND);
            microClient.send(microMessage);
            System.out.println("send client message : " + message);
            microClient.close();
        }
        System.in.read();
    }
}
