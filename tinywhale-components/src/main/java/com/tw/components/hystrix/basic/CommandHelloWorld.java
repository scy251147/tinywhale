package com.tw.components.hystrix.basic;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-16 20:10
 */
public class CommandHelloWorld extends HystrixCommand<String> {

    private final String name;

    protected CommandHelloWorld(String name) {
        super(HystrixCommandGroupKey.Factory.asKey("ExampleGroup"));
        this.name = name;
    }

    @Override
    protected String run() throws Exception {
        //实际上，这里一般会做一些网络调用
        throw new Exception("xxx");
//        return "Hello " + name + "!";
    }

    @Override
    protected String getFallback() {
        return "Hello Failure" + name + "!";
    }
}
