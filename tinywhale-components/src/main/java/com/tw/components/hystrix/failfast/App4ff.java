package com.tw.components.hystrix.failfast;

import com.netflix.hystrix.exception.HystrixRuntimeException;

/**
 * @author shichaoyang
 * @Description: 快速失败,相当于出现异常会直接抛出，不会吧失败信息吃掉
 * @date 2021-03-17 11:03
 */
public class App4ff {

    public static void main(String... args) throws Exception {
        success();
        fail();
    }

    private static void success(){
        String rst =  new CommandThatFailsFast(false).execute();
        System.out.println(rst);
    }

    private static void fail(){
        try{
            String rst = new CommandThatFailsFast(true).execute();
            System.out.println(rst);
        }catch(HystrixRuntimeException e){
            e.printStackTrace();
        }
    }
}
