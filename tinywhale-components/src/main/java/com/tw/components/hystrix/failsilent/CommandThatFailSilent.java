package com.tw.components.hystrix.failsilent;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

import java.util.Collections;
import java.util.List;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-03-17 11:23
 */
public class CommandThatFailSilent  extends HystrixCommand<String> {

    public CommandThatFailSilent(boolean throwException){
        super(HystrixCommandGroupKey.Factory.asKey(GROUP));
        this.throwException = throwException;
    }

    private static final String GROUP = "failSilentGroup";

    private final boolean throwException;

    @Override
    protected String run() throws Exception {
        if(throwException){
            throw new RuntimeException("failure from CommandThatFailSilent");
        }else{
            return "success";
        }
    }

    @Override
    protected String getFallback(){
        return null;
    }

}
