package com.tw.components.hystrix.failsilent;

import com.netflix.hystrix.exception.HystrixRuntimeException;

/**
 * @author shichaoyang
 * @Description: 故障沉默, 吞掉了故障，不做任何响应
 * @date 2021-03-17 11:21
 */
public class App4fs {

    public static void main(String...args) throws Exception{

        success();
        fail();

    }

    private static void success(){
        String rst = new CommandThatFailSilent(false).execute();
        System.out.println(rst);
    }

    private static void fail(){
        try{
            String rst = new CommandThatFailSilent(true).execute();
            System.out.println(rst);
        }catch(HystrixRuntimeException e){
            System.out.println("we should not get an exception as we fail silently with a fallback");
        }
    }

}
