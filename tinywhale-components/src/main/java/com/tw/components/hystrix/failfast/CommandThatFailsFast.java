package com.tw.components.hystrix.failfast;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

/**
 * @author shichaoyang
 * @Description: CommandThatFailsFast
 * @date 2021-03-17 11:04
 */
public class CommandThatFailsFast extends HystrixCommand<String> {

    public CommandThatFailsFast(boolean throwException) {
        super(HystrixCommandGroupKey.Factory.asKey(GROUP));
        this.throwException = throwException;
    }

    private static final String GROUP = "FailFastGroup";

    private final boolean throwException;

    @Override
    protected String run() throws Exception {
        if (throwException) {
            throw new RuntimeException("failure from CommandThatFailsFast");
        } else {
            return "success";
        }
    }
}
