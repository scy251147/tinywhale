package com.tw.components.hystrix.basic;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author shichaoyang
 * @Description: Circuit Breaker by using hystrix
 * hystrix熔断器  @see https://github.com/Netflix/Hystrix/wiki/How-To-Use#MigratingLibrary
 *
 * 可以做断路器，failfast，failsilent，fallback等等，具体资料可以自己翻阅
 *
 * @date 2021-03-16 20:03
 */
public class App4h {

    public static void main(String... args) throws Exception {
        exec3();
    }

    private static void exec1(){
        String s = new CommandHelloWorld("Bob").execute();
        System.out.println(s);
    }

    private static void exec2() throws ExecutionException, InterruptedException {
        Future<String> s = new CommandHelloWorld("Bob").queue();
        System.out.println(s.get());
    }

    private static void exec3() {
        rx.Observable<String> s = new CommandHelloWorld("Bob").observe();
        s.subscribe(
                item -> {
                    System.out.println(item);
                }
                , exception -> {
                    System.out.println(exception);
                });
    }

}
