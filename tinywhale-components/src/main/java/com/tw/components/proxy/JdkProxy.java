package com.tw.components.proxy;

import com.tw.components.micro.MicroClient;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.lang.reflect.Proxy;

/**
 * @author shichaoyang
 * @Description: jdk反射代理
 * @date 2020-06-17 16:10
 */
@Service
public class JdkProxy {

    @Resource
    private MicroClient microClient;

    @Value("${components.server.host}")
    private String host;

    @Value("${components.server.port}")
    private int port;

    public void init() throws InterruptedException {
        microClient.Connect(host,port);
    }

    /**
     * 创建客户端代理
     *
     * @param interfaceClass
     * @param <T>
     * @return
     */
    public <T> T create(Class<?> interfaceClass) {
        T result = (T) Proxy.newProxyInstance(interfaceClass.getClassLoader()
                , new Class<?>[]{interfaceClass}
                , (proxy, method, args) -> {
                    MicroMessage microMessage = new MicroMessage();
                    microMessage.setType(MicroType.COMMAND);
                    microMessage.setMessage(args[0].toString());
                    microClient.send(microMessage);
                    return "ok";
                });
        return result;
    }
}
