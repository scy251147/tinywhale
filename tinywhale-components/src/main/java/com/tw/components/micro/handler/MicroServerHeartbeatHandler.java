package com.tw.components.micro.handler;

import com.tw.components.micro.domain.Heartbeat;
import com.tw.components.micro.domain.HeartbeatPool;
import com.tw.components.micro.helper.ConTrace;
import com.tw.components.micro.helper.LogTrace;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author shichaoyang
 * @Description: 服务端心跳包处理
 * @date 2020-06-11 9:27
 */
public class MicroServerHeartbeatHandler extends ChannelInboundHandlerAdapter {

    //日志记录
    private static final Logger logger = LoggerFactory.getLogger(MicroServerHeartbeatHandler.class);

    /**
     * 客户端注册
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.error("客户端上线:" + ctx.channel());
        resetHeartbeatCounter(ctx);
    }

    /**
     * 客户端注销
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.error("客户端下线:" + ctx.channel());
        removeHeartbeatCounter(ctx);
    }

    /**
     * 消息读取
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        MicroMessage message = (MicroMessage) msg;
        if (message != null && message.getType() == MicroType.HEARTBEAT) {
            resetHeartbeatCounter(ctx);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    /**
     * 自触发事件处理
     * @param ctx
     * @param evt
     * @throws Exception
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            //读空闲处理
            if (event.state() == IdleState.READER_IDLE) {
                readIdleHandle(ctx);
            }
            //写空闲处理
            else if (event.state() == IdleState.WRITER_IDLE) {
                writeIdleHandle(ctx);
            }
            //读写均空闲处理
            else if (event.state() == IdleState.ALL_IDLE) {
                allIdleHandle(ctx);
            }
        }
    }

    /**
     * 重置客户端心跳计数
     * @param ctx
     */
    private void resetHeartbeatCounter(ChannelHandlerContext ctx) {
        if (LogTrace.logEnable()) {
            logger.error("重置客户端[" + ctx.channel().remoteAddress().toString() + "]心跳, 计数清零");
        }
        String channelId = ctx.channel().id().asLongText();
        HeartbeatPool.insert(channelId, ctx.channel(), 0);
    }

    /**
     * 移除客户端心跳计数
     * @param ctx
     */
    private void removeHeartbeatCounter(ChannelHandlerContext ctx){
        String channelId = ctx.channel().id().asLongText();
        HeartbeatPool.remove(channelId);
        if(ctx.channel().isActive()){
            ctx.channel().close();
        }
    }

    /**
     * 开始心跳计数，计数超过3次，认为客户端下线
     * @param ctx
     */
    private void startHeatbeatCounter(ChannelHandlerContext ctx) {
        String channelId = ctx.channel().id().asLongText();
        Heartbeat heartbeat = HeartbeatPool.get(channelId);
        int times = heartbeat.getCounter().getAndIncrement();
        Channel channel = heartbeat.getChannel();
        if (times > 3) {
            if (ConTrace.reconnect()) {
                logger.error("客户端[" + ctx.channel().remoteAddress().toString() + "]已经掉线，进行重连...");
                try {
                    if (!channel.isActive()) {
                        ChannelFuture future = channel.connect(channel.remoteAddress()).sync();
                        if (future.isDone()) {
                            logger.error("客户端[" + ctx.channel().remoteAddress().toString() + "]重连成功...");
                        }
                    } else {
                        logger.error("客户端[" + ctx.channel().remoteAddress().toString() + "]实际未掉线，无需重连...");
                        resetHeartbeatCounter(ctx);
                    }
                } catch (Exception e) {
                    logger.error("客户端[" + ctx.channel().remoteAddress().toString() + "]重连失败，移出队列...");
                    removeHeartbeatCounter(ctx);
                }
            } else {
                logger.error("客户端[" + ctx.channel().remoteAddress().toString() + "]已经掉线，断开连接...");
                ctx.channel().close();
            }
        }
    }

    /**
     * 读空闲超过45秒，命中此函数
     * @param ctx
     */
    private void readIdleHandle(ChannelHandlerContext ctx) {
        startHeatbeatCounter(ctx);
    }

    /**
     * 写空闲超过45秒，命中此函数
     * @param ctx
     */
    private void writeIdleHandle(ChannelHandlerContext ctx){
        startHeatbeatCounter(ctx);
    }

    /**
     * 读写均空闲超过120秒，命中此函数
     * @param ctx
     */
    private void allIdleHandle(ChannelHandlerContext ctx){

    }

}
