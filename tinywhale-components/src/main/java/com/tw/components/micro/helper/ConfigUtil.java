package com.tw.components.micro.helper;

import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-16 15:48
 */
public class ConfigUtil {

    private static final String CONFIGURATION_FILE = "/properties/project.properties";

    private static final Properties properties;

    static {
        properties = new Properties();
        try (InputStream inputStream = Configuration.class.getResourceAsStream(CONFIGURATION_FILE)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read file " + CONFIGURATION_FILE, e);
        }
    }

    /**
     * 获取配置
     * @param key
     * @return
     */
    public String get(String key) {
        return properties.getProperty(key);
    }
}
