package com.tw.components.micro.handler;

import com.tw.components.micro.helper.LogTrace;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author shichaoyang
 * @Description: 客户端心跳包处理
 * @date 2020-06-11 9:27
 */
public class MicroClientHeartbeatHandler extends ChannelInboundHandlerAdapter {

    //日志记录
    private static final Logger logger = LoggerFactory.getLogger(MicroClientHeartbeatHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        MicroMessage message = (MicroMessage) msg;
        if (message != null && message.getType() == MicroType.HEARTBEAT) {
            if(LogTrace.logEnable()) {
                logger.error("收到服务端[" + ctx.channel().remoteAddress().toString() + "]心跳");
            }
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            //读空闲处理
            if (event.state() == IdleState.READER_IDLE) {
                if (LogTrace.logEnable()) {
                    logger.error("读空闲，发送心跳包给服务器[" + ctx.channel().remoteAddress() + "]");
                }
                ctx.writeAndFlush(MicroMessage.buildHeartbeatMessage());
            }
            //写空闲处理
            else if (event.state() == IdleState.WRITER_IDLE) {
                if (LogTrace.logEnable()) {
                    logger.error("写空闲，发送心跳包给服务器[" + ctx.channel().remoteAddress() + "]");
                }
                ctx.writeAndFlush(MicroMessage.buildHeartbeatMessage());
            }
            //读写均空闲处理
            else if (event.state() == IdleState.ALL_IDLE) {
            }
        }
    }
}
