package com.tw.components.micro.codec;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-10 18:26
 */
public class MicroLengthDecoder extends LengthFieldBasedFrameDecoder {
    public MicroLengthDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength);
    }
}
