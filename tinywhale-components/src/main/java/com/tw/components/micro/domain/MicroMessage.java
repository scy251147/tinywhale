package com.tw.components.micro.domain;

import java.util.UUID;

/**
 * @author shichaoyang
 * @Description: 消息体
 * @date 2020-06-10 10:06
 */
public class MicroMessage {

    //请求id
    private String requestId;

    //消息内容
    private String message;

    //消息类型
    private MicroType type;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MicroType getType() {
        return type;
    }

    public void setType(MicroType type) {
        this.type = type;
    }

    /**
     * 构建心跳消息
     * @return
     */
    public static MicroMessage buildHeartbeatMessage(){
        MicroMessage microMessage = new MicroMessage();
        microMessage.setType(MicroType.HEARTBEAT);
        return microMessage;
    }

    /**
     * 构建登录消息
     * @param socket
     * @return
     */
    public static MicroMessage buildLoginMessage(String socket){
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setType(MicroType.LOGIN);
        microMessage.setMessage(socket);
        return microMessage;
    }

    /**
     * 构建登出消息
     * @param socket
     * @return
     */
    public static MicroMessage buildLogoutMessgae(String socket){
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setType(MicroType.LOGOUT);
        microMessage.setMessage(socket);
        return microMessage;
    }

    /**
     * 构建交流信息
     * @param command
     * @return
     */
    public static MicroMessage buildCommandMessage(String command){
        MicroMessage microMessage = new MicroMessage();
        microMessage.setRequestId(UUID.randomUUID().toString());
        microMessage.setType(MicroType.COMMAND);
        microMessage.setMessage(command);
        return microMessage;
    }
}
