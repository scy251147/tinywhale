package com.tw.components.micro.domain;

import io.netty.channel.Channel;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author shichaoyang
 * @Description: 心跳
 * @date 2020-06-17 11:10
 */
public class Heartbeat {

    //客户端通道
    private Channel channel;

    //客户端计数
    private AtomicInteger counter;

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public AtomicInteger getCounter() {
        return counter;
    }

    public void setCounter(AtomicInteger counter) {
        this.counter = counter;
    }
}
