package com.tw.components.micro.handler;

import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.concurrent.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-10 10:03
 */
@ChannelHandler.Sharable
public class MicroClientHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(MicroClientHandler.class);

    public MicroClientHandler(Map<String, Promise<MicroMessage>> messageQueue){
        this.messageQueue = messageQueue;
    }

    private Map<String, Promise<MicroMessage>> messageQueue;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        MicroMessage message = (MicroMessage) msg;
        if (message != null && message.getType() == MicroType.COMMAND) {
            //logger.error("收到服务端[" + ctx.channel().remoteAddress().toString() + "]推送消息 : " + message.getMessage());
            if (messageQueue != null && messageQueue.size() > 0) {
                messageQueue.get(message.getRequestId()).setSuccess(message);
                messageQueue.remove(message.getRequestId());
                //logger.error(messageQueue.size()+"");
            }
        }else{
            ctx.fireChannelRead(msg);
        }
    }
}
