package com.tw.components.micro.disruptor;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.tw.components.micro.domain.PublishMessage;

/**
 * @author shichaoyang
 * @Description: 启动构建器
 * @date 2020-06-28 18:12
 */
public class MicroBuilder {

    private static MicroProducer MICROPRODUCER;

    /**
     * 构建器
     *
     * @return
     */
    public static MicroProducer getInstance() {
        if (MICROPRODUCER != null) {
            return MICROPRODUCER;
        }
        MicroFactory microFactory = new MicroFactory();
        Disruptor<PublishMessage> disruptor = new Disruptor<>(microFactory, 1024, r -> {
            return new Thread(r);
        });

        disruptor.handleEventsWithWorkerPool(new MicroConsumer());
        disruptor.start();

        RingBuffer<PublishMessage> ringBuffer = disruptor.getRingBuffer();
        MicroProducer microProducer = new MicroProducer(ringBuffer);
        MICROPRODUCER = microProducer;
        return microProducer;
    }

}
