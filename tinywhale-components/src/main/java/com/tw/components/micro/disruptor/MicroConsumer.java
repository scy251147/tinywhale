package com.tw.components.micro.disruptor;

import com.lmax.disruptor.WorkHandler;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.PublishMessage;
import com.tw.components.micro.helper.MsgTrace;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author shichaoyang
 * @Description: 消费端
 * @date 2020-06-28 17:17
 */
public class MicroConsumer implements WorkHandler<PublishMessage> {

    private static final Logger logger = LoggerFactory.getLogger(MicroConsumer.class);

    @Override
    public void onEvent(PublishMessage publishMessage) throws Exception {
        MicroMessage message = publishMessage.getMicroMessage();
        ChannelHandlerContext ctx = publishMessage.getCtx();

        logger.error("收到客户端[" + ctx.channel().remoteAddress().toString() + "]推送消息 : " + message.getMessage() + " 消息条数：" + MsgTrace.counter.incrementAndGet() + " 线程：" + Thread.currentThread().getName());

        ctx.writeAndFlush(publishMessage.getMicroMessage());
    }
}
