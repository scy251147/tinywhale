package com.tw.components.micro.disruptor;

import com.lmax.disruptor.RingBuffer;
import com.tw.components.micro.domain.PublishMessage;

/**
 * @author shichaoyang
 * @Description: 生产端
 * @date 2020-06-28 17:17
 */
public class MicroProducer {

    public MicroProducer(RingBuffer<PublishMessage> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    private RingBuffer<PublishMessage> ringBuffer;

    public void publish(PublishMessage publishMessage) {
        long sequence = ringBuffer.next();

        //TODO 缓冲池的检查
        // if capacity less than 10%, don't use ringbuffer anymore
        //if(ringBuffer.remainingCapacity() < RING_SIZE * 0.1) {
        //    log.warn("disruptor:ringbuffer avaliable capacity is less than 10 %");
        //    return;
        //}

        try {
            PublishMessage newMessage = ringBuffer.get(sequence);
            newMessage.setMicroMessage(publishMessage.getMicroMessage());
            newMessage.setCtx(publishMessage.getCtx());
        } finally {
            ringBuffer.publish(sequence);
        }
    }
}
