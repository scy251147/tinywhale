package com.tw.components.micro.domain;

import io.netty.channel.ChannelHandlerContext;

/**
 * @author shichaoyang
 * @Description: disruptor生产数据
 * @date 2020-06-28 18:25
 */
public class PublishMessage {

    private ChannelHandlerContext ctx;

    private MicroMessage microMessage;

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public MicroMessage getMicroMessage() {
        return microMessage;
    }

    public void setMicroMessage(MicroMessage microMessage) {
        this.microMessage = microMessage;
    }
}
