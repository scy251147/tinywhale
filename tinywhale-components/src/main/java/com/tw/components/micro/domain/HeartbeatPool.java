package com.tw.components.micro.domain;

import io.netty.channel.Channel;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author shichaoyang
 * @Description: 心跳池
 * @date 2020-06-17 11:11
 */
public class HeartbeatPool {

    //心跳池
    private static Map<String, Heartbeat> heartbeatPool = new ConcurrentHashMap();

    /**
     * 从心跳池获取客户端心跳信息
     * @param key
     * @return
     */
    public static Heartbeat get(String key){
        return heartbeatPool.get(key);
    }

    /**
     * 设置客户端心跳信息到心跳池
     * @param key
     * @param channel
     * @param count
     */
    public static void insert(String key, Channel channel, int count) {
        if (!heartbeatPool.containsKey(key)) {
            Heartbeat heartbeat = new Heartbeat();
            heartbeat.setChannel(channel);

            AtomicInteger counter = new AtomicInteger(count);
            heartbeat.setCounter(counter);
            heartbeatPool.put(key, heartbeat);
        }
        else{
            heartbeatPool.get(key).setChannel(channel);
            heartbeatPool.get(key).getCounter().set(count);
        }
    }

    /**
     * 移除客户端心跳信息
     * @param key
     */
    public static void remove(String key){
        heartbeatPool.remove(key);
    }


}
