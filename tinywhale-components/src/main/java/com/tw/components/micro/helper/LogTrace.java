package com.tw.components.micro.helper;

import org.springframework.util.StringUtils;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-16 15:43
 */

public class LogTrace {

    private static final String LOG_PRINT_CONFIG_KEY = "components.log.enable";

    private static final ConfigUtil CONFIG_UTIL = new ConfigUtil();

    /**
     * 日志打印
     * @return
     */
    public static boolean logEnable() {
        String val = CONFIG_UTIL.get(LOG_PRINT_CONFIG_KEY);
        if (StringUtils.isEmpty(val)) {
            return false;
        }
        if (val.equals("1")) {
            return true;
        }
        return false;
    }
}
