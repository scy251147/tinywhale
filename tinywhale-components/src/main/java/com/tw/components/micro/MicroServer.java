package com.tw.components.micro;

import com.tw.components.micro.codec.MicroLengthDecoder;
import com.tw.components.micro.codec.MicroMessageDecoder;
import com.tw.components.micro.codec.MicroMessageEncoder;
import com.tw.components.micro.disruptor.MicroBuilder;
import com.tw.components.micro.disruptor.MicroProducer;
import com.tw.components.micro.handler.MicroServerHandler;
import com.tw.components.micro.handler.MicroServerHeartbeatHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-10 9:32
 */
@Service
public class MicroServer {

    //日志记录
    private static final Logger logger = LoggerFactory.getLogger(MicroServer.class);

    //主事件池
    private EventLoopGroup bossGroup = new NioEventLoopGroup();

    //副事件池
    private EventLoopGroup workerGroup = new NioEventLoopGroup();

    //服务端通道
    private Channel serverChannel;

    //disruptor队列
    private MicroProducer microProducer = MicroBuilder.getInstance();

    /**
     * 绑定本机监听
     *
     * @throws Exception
     */
    public void Start(String host, int port) throws Exception {

        //启动器
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        //为Acceptor设置事件池，为客户端接收设置事件池
        serverBootstrap.group(bossGroup, workerGroup)
                //工厂模式，创建NioServerSocketChannel类对象
                .channel(NioServerSocketChannel.class)
                //等待队列大小
                .option(ChannelOption.SO_BACKLOG, 100)
                //地址复用
                .option(ChannelOption.SO_REUSEADDR, true)
                //日志记录组件的level
                .handler(new LoggingHandler(LogLevel.INFO))
                //各种业务处理handler
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) throws Exception {
                        //空闲检测handler，用于检测通道空闲状态
                        channel.pipeline().addLast("idleStateHandler", new IdleStateHandler(5, 5, 120));
                        //流量整形
                        channel.pipeline().addLast("trafficShapping", new ChannelTrafficShapingHandler(10 * 1024 * 1024 * 1024, 10 * 1024 * 1024 * 1024, 1000));
                        //拆分消息，头部字节长度为4字节
                        channel.pipeline().addLast("nettyLengthDecoder", new MicroLengthDecoder(1024 * 1024, 0, 4));
                        channel.pipeline().addLast("nettyMessageEncoder", new MicroMessageEncoder());
                        channel.pipeline().addLast("nettyMessageDecoder", new MicroMessageDecoder());
                        //业务hander，放到编解码后面
                        channel.pipeline().addLast("heartBeatHandler", new MicroServerHeartbeatHandler());
                        channel.pipeline().addLast("nettyHandler", new MicroServerHandler(microProducer));
                    }
                });

        //绑定端口，同步等待成功
        ChannelFuture future = serverBootstrap.bind(host, port).sync();

        //注册连接事件监听器
        future.addListener(cfl -> {
            if (cfl.isSuccess()) {
                logger.info("服务端[" + host + ":" + port + "]已上线...");
                serverChannel = future.channel();
            }
        });

        //注册关闭事件监听器
        future.channel().closeFuture().addListener(cfl -> {
            //关闭服务端
            close();
            logger.info("服务端[" + host + ":" + port + "]已下线...");
        });
    }

    /**
     * 关闭server
     */
    public void close() {
        //关闭套接字
        if(serverChannel!=null){
            serverChannel.close();
        }
        //关闭主线程组
        if (bossGroup != null) {
            bossGroup.shutdownGracefully();
        }
        //关闭副线程组
        if (workerGroup != null) {
            workerGroup.shutdownGracefully();
        }
    }

}
