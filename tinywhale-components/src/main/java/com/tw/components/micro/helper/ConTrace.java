package com.tw.components.micro.helper;

import org.springframework.util.StringUtils;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-17 10:27
 */
public class ConTrace {

    private static final String CLIENT_RECONNECT_ENABLE = "components.reconnect.enable";

    private static final ConfigUtil CONFIG_UTIL = new ConfigUtil();

    /**
     * 重连客户端
     * @return
     */
    public static boolean reconnect() {
        String val = CONFIG_UTIL.get(CLIENT_RECONNECT_ENABLE);
        if (StringUtils.isEmpty(val)) {
            return false;
        }
        if (val.equals("1")) {
            return true;
        }
        return false;
    }

}
