package com.tw.components.micro.handler;

import com.tw.components.micro.disruptor.MicroProducer;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.domain.MicroType;
import com.tw.components.micro.domain.PublishMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-10 10:00
 */
public class MicroServerHandler  extends ChannelInboundHandlerAdapter {

    //日志
    private static final Logger logger = LoggerFactory.getLogger(MicroServerHandler.class);

    //带参构造
    public MicroServerHandler(MicroProducer microProducer){
        this.microProducer = microProducer;
    }


    //disruptor生产
    private MicroProducer microProducer;

    //将业务处理利用disruptor解耦出去，以便于提高性能
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        MicroMessage message = (MicroMessage) msg;
        if (message != null && message.getType() == MicroType.COMMAND) {
            PublishMessage publishMessage = new PublishMessage();
            publishMessage.setCtx(ctx);
            publishMessage.setMicroMessage(message);
            microProducer.publish(publishMessage);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("服务端异常，服务端Socket[" + ctx.channel().localAddress().toString() + "], 客户端Socket[" + ctx.channel().remoteAddress().toString() + "]", cause);
        ctx.channel().close();
    }
}
