package com.tw.components.micro.domain;

/**
 * @author shichaoyang
 * @Description: 通信种类
 * @date 2020-06-11 9:30
 */
public enum MicroType {

    LOGIN(0x01, "登入"),
    LOGOUT(0x02, "登出"),
    HEARTBEAT(0x03, "心跳"),
    COMMAND(0x04, "消息交流");

    private int key;

    private String val;

    MicroType(int key, String val) {
        this.key = key;
        this.val = val;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
