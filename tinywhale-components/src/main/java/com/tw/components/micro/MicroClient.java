package com.tw.components.micro;

import com.tw.components.micro.codec.MicroLengthDecoder;
import com.tw.components.micro.helper.LogTrace;
import com.tw.components.micro.domain.MicroMessage;
import com.tw.components.micro.codec.MicroMessageDecoder;
import com.tw.components.micro.codec.MicroMessageEncoder;
import com.tw.components.micro.handler.MicroClientHandler;
import com.tw.components.micro.handler.MicroClientHeartbeatHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-10 9:32
 */
@Service
public class MicroClient {

    //日志记录
    private static final Logger logger = LoggerFactory.getLogger(MicroClient.class);

    //事件池
    private EventLoopGroup group = new NioEventLoopGroup();

    //启动器
    private Bootstrap bootstrap = new Bootstrap();

    //客户端通道
    private Channel clientChannel;

    //客户端处理handler
    private MicroClientHandler microClientHandler;

    //客户端消息队列
    private Map<String, Promise<MicroMessage>> messageQueue = new ConcurrentHashMap<>();

    /**
     * 初始化设置
     */
    @PostConstruct
    public void init() {
        microClientHandler = new MicroClientHandler(messageQueue);
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                //日志记录组件的level
                .handler(new LoggingHandler(LogLevel.INFO))
                .option(ChannelOption.SO_REUSEADDR, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) throws Exception {
                        //空闲检测handler，用于检测通道空闲状态
                        channel.pipeline().addLast("idleStateHandler", new IdleStateHandler(5, 5, 120));
                        //拆分消息，头部字节长度为4字节
                        channel.pipeline().addLast("nettyLengthDecoder", new MicroLengthDecoder(1024 * 1024, 0, 4));
                        channel.pipeline().addLast("nettyMessageEncoder", new MicroMessageEncoder());
                        channel.pipeline().addLast("nettyMessageDecoder", new MicroMessageDecoder());
                        //业务hander，放到编解码后面
                        channel.pipeline().addLast("heartBeatHandler", new MicroClientHeartbeatHandler());
                        channel.pipeline().addLast("clientHandler", microClientHandler);
                    }
                });
    }

    /**
     * 连接服务器
     *
     * @param host
     * @param port
     * @throws InterruptedException
     */
    public void Connect(String host, int port) throws InterruptedException {

        //发起同步连接操作
        ChannelFuture channelFuture = bootstrap.connect(host, port).sync();

        //检测连接完毕
        if (channelFuture.isDone() && channelFuture.isSuccess()) {
            if (LogTrace.logEnable()) {
                logger.info("客户端[" + channelFuture.channel().localAddress().toString() + "]已连接...");
            }
            clientChannel = channelFuture.channel();
            //高水位设置，防止客户端不停发送而产生OOM
            clientChannel.config().setWriteBufferHighWaterMark(10 * 1024 * 1024);
        } else {
            logger.info("客户端[" + channelFuture.channel().localAddress().toString() + "]尝试重连...");
            //尝试重新连接
            Connect(host, port);
        }

        //注册关闭事件
        channelFuture.channel().closeFuture().addListener(cfl -> {
            close();
            if (LogTrace.logEnable()) {
                logger.info("客户端[" + channelFuture.channel().localAddress().toString() + "]已断开...");
            }
        });
    }

    /**
     * 客户端关闭
     */
    public void close() {
        try {
            //关闭客户端套接字
            if (clientChannel != null) {
                clientChannel.close().awaitUninterruptibly();
            }
            //此处将group关闭，将会使得服务端抛错，远程主机强迫关闭了一个连接xxxxxxxx并抛出exception，会被server端的exceptioncaught捕捉
            if (group != null) {
                group.shutdownGracefully();
            }
        } catch (Exception e) {
            logger.error("客户端关闭失败...", e);
        }
    }

    /**
     * 客户端发送信息，不带返回标记
     *
     * @param microMessage
     */
    public boolean send(MicroMessage microMessage) {

        //通道未准备好
        if (clientChannel == null) {
            logger.error("ctx is not prepared well now...");
            return false;
        }

        //当前通道不可写
        if(!clientChannel.isWritable()){
            logger.error("hit high water mark...");
            return false;
        }

        clientChannel.writeAndFlush(microMessage);
        return true;
    }


    /**
     * 客户端同步发送消息并返回
     *
     * @param microMessage
     * @return
     */
    public MicroMessage syncSend(MicroMessage microMessage) throws Exception {
        Promise<MicroMessage> promise = asyncSend(microMessage);
        if (promise.isSuccess()) {
            MicroMessage rst = promise.get(3, TimeUnit.SECONDS);
            return rst;
        } else {
            return null;
        }
    }

    /**
     * 客户端异步发送消息并返回
     * @param microMessage
     * @return
     */
    public Promise<MicroMessage> asyncSend(MicroMessage microMessage) {

        Promise<MicroMessage> promise = new DefaultPromise<>(new DefaultEventExecutor());

        //通道未准备好
        if (clientChannel == null) {
            logger.error("ctx is not prepared well now...");
            return promise.setFailure(new Exception("通道未准备好..."));
        }

        //当前通道不可写
        if (!clientChannel.isWritable()) {
            logger.error("hit high water mark...");
            return promise.setFailure(new Exception("命中高水位限制..."));
        }


        //数据异步发送
        if (!messageQueue.containsKey(microMessage.getRequestId())) {
            messageQueue.put(microMessage.getRequestId(), promise);
            clientChannel.writeAndFlush(microMessage);
        }

        return promise;
    }
}
