package com.tw.components.micro.disruptor;

import com.lmax.disruptor.EventFactory;
import com.tw.components.micro.domain.PublishMessage;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-28 17:57
 */

public class MicroFactory implements EventFactory<PublishMessage> {

    @Override
    public PublishMessage newInstance() {
        return new PublishMessage();
    }
}