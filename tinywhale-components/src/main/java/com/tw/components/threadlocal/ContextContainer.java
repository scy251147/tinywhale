package com.tw.components.threadlocal;


import com.alibaba.fastjson.JSON;

/**
 * @author shichaoyang
 * @Description: threadlocal类自封装
 * @date 2021-01-19 16:50
 */
public class ContextContainer<T> {

    private static ThreadLocal threadLocal = new ThreadLocal();

    private ContextContainer(Builder builder) {
        this.containerName = builder.containerName;
        this.autoRemove = builder.autoRemove;
    }

    private String containerName;

    private boolean autoRemove;

    public static class Builder {

        private String containerName;

        private boolean autoRemove = false;

        public Builder setContainerName(String containerName){
            this.containerName = containerName;
            return this;
        }

        public Builder setAutoRemove(boolean autoRemove){
            this.autoRemove = autoRemove;
            return this;
        }

        public ContextContainer build() {
            return new ContextContainer(this);
        }
    }

    /**
     * 设置
     * @param component
     */
    public void set(T component) {
        threadLocal.set(component);
    }

    /**
     * 获取
     * @return
     */
    public T get() {
        if (threadLocal == null) {
            return null;
        }
        T result = (T) threadLocal.get();
        if (autoRemove) {
            del();
        }
        return result;
    }

    /**
     * 删除
     */
    public void del() {
        threadLocal.remove();
    }

    public String toString() {
        return "ContainerName:" + containerName +
                " AutoRemove:" + autoRemove +
                " Params:" + JSON.toJSONString(threadLocal == null ? "" : threadLocal.get())
                ;
    }
}
