package com.tw.components.threadlocal;

/**
 * @author shichaoyang
 * @Description:
 * @date 2021-01-21 11:13
 */
public class ContextParam<T> {

    private T params;

    public T getParams() {
        return params;
    }

    public void setParams(T params) {
        this.params = params;
    }
}
