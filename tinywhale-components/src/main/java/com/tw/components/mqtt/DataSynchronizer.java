package com.tw.components.mqtt;

/**
 * @author shichaoyang
 * @Description: 数据同步器
 * @date 2020-10-14 19:41
 */
public class DataSynchronizer {

    /**
     * 加载本地底层C实现库
     */
    static {
        System.loadLibrary("synchronizer");
    }

    /**
     * 底层数据同步方法
     */
    private native boolean syncData(int status);

    /**
     * 程序启动，调用底层数据同步方法
     *
     * @param args
     */
    public static void main(String... args) {
        boolean rst = new DataSynchronizer().syncData(999);
        System.out.println("The execute result from C is : " + rst);
    }

}
