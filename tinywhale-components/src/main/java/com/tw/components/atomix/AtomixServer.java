package com.tw.components.atomix;

/**
 * @author shichaoyang
 * @Description: Atomix 2.1 is a fully featured framework for building fault-tolerant distributed systems.
 * Combining ZooKeeper’s consistency with Hazelcast’s usability and performance, Atomix uses a set of custom communication APIs, a sharded Raft cluster, and a multi-primary protocol to provide a series of high-level primitives for building distributed systems. These primitives include:
 * Cluster management and failure detection
 * Cluster communication (direct and pub-sub) via Netty
 * Strongly consistent reactive distributed coordination primitives (locks, leader elections, etc)
 * Efficient partitioned distributed data structures (maps, sets, trees, etc)
 * Standalone Agent
 * REST API
 * Interactive CLI
 * @Ref https://atomix.io/docs/latest/getting-started/
 * @date 2020-06-29 20:58
 */
public class AtomixServer {



}
