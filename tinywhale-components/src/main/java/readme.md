
1. 客户端一般用setWriteBufferHighWaterMark来保护客户端，不致因为发送速度过快而导致oom的问题；服务端一般用流控来进行限流。
   可以参考：https://www.jianshu.com/p/6c4a7cbbe2b5

2. 服务端业务处理，一般使用多线程将其解耦出去，防止阻塞netty主线程处理速度。由于多线程一般是带锁的queue，性能不是很好，这里可以使用disruptor进行解耦。

3. 断线重连可以根据心跳包模式，来进行具体实现：
   a. 心跳包是client->server, 再由server->client， 则可以在client端进行重连
   b. 心跳包是client->server, 则可以在server端进行重连
   c. 心跳包是server->client, 则可以在client端进行重连
   
4. 粘包拆包处理，必须从LengthFieldBasedFrameDecoder，LineBasedFrameDecoder，DelimiterBasedFrameDecoder，FixedLengthFrameDecoder中选择一个来进行，
   单独的StringDecoder无法很好的进行粘包拆包处理，只能结合上面几种一起使用。

5. Promise，Future等的使用，netty中过于简单，需要自己尝试实现。

6. 短连接性能极差，长连接性能极好，任何时候都应该保持长连接。

7. c10k问题，一般是操作系统层面进行调节，比如linux中修改epoll=true, 文件句柄为100000等，代码层面上不需要特别的编写什么。
   可以参考：https://www.jianshu.com/p/54f9bfcd054b
   
8. netty有完善的指标数据采集。
   可以参考：https://www.jianshu.com/p/4c3b5d012ed1