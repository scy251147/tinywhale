import com.tw.components.micro.MicroServer;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;

/**
 * @author shichaoyang
 * @Description:
 * @date 2020-06-15 15:39
 */
public class Bootstrap {

    @Resource
    private MicroServer microServer;

    @Value("${components.server.host}")
    private String host;

    @Value("${components.server.port}")
    private int port;

    public void start() throws Exception {
        System.out.println("start");
        microServer.Start(host, port);
    }
}
