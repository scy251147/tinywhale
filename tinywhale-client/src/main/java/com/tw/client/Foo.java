package com.tw.client;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shichaoyang
 * @Description: 实现方法，注意package一定要删掉，否则无法运行成功
 * @date 2021-03-08 17:39
 */
public class Foo{

    public void sayHello(){
        System.out.println("hello world22222! (version 11)");

        new Thread(()->{
            System.out.print("in child thread");
            List<String> list = new ArrayList();
            list.add("1");
        }).start();
    }

}
