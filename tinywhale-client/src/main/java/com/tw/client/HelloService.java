package com.tw.client;

/**
 * @author shichaoyang
 * @Description:
 * @date 2019-02-20 21:13
 */
public interface HelloService {

    String hello(String name);

}
